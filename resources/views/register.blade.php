@extends('doctype')

@section('doctype')
    @parent
    <link rel="stylesheet" href="{{  asset('css/register.css')}}">
@endsection
</head>
<body>
	<div id="main">
		<h1 class="text-center m-2 text-info ">SIGN UP</h1>
		<form action="{{url('/post')}}" method="post" class="form-group w-75 m-auto">
			{{ csrf_field() }}
			<!-- es partadir banali dasht e -->
			<div class="form-group mb-4 text-white">
				<input class="form-control" type="text" name="name" placeholder="Name" value="{{ old('name') }}">
			{!! $errors->first('name', '<p class="err p-0 alert alert-danger help-block">:message</p>') !!}
			</div>

			<div class="form-group mb-4 text-white">
				<input class="form-control" type="text" name="surname" placeholder="Surname" value="{{old('surname')}}">
			{!! $errors->first('surname', '<p class="err p-0 alert alert-danger help-block">:message</p>') !!}
			</div>
			<div class="form-group mb-4 text-white">
				<input class="form-control" type="text" name="email" placeholder="Email" value="{{old('email')}}">
			{!! $errors->first('email', '<p class="err p-0 alert alert-danger help-block">:message</p>') !!}
			</div>
			<div class="form-group mb-4 text-white">
				<input class="form-control" type="password" name="password" placeholder="Paswword">
			{!! $errors->first('password', '<p class="err p-0 alert alert-danger help-block">:message</p>') !!}

			</div>
			<div class="form-group mb-4 text-white">
				<input class="form-control" type="password" name="password_confirmation" placeholder="Repassword">
			{!! $errors->first('password_confirmation', '<p class="err p-0 alert alert-danger help-block">:message</p>') !!}

			</div>
			<div class="form-group mb-4 text-center d-flex justify-content-between">

				<select class="btn btn-secondary" name="day">
					<option selected="" disabled="">Day</option>
					@for ($i = 1; $i <=31; $i++)
                          <option>{{ $i }}</option>
                    @endfor
				</select>

				<select  class="btn btn-secondary" name="month">
					<option selected="" disabled="">Month</option>
					<option value="01" >January</option>
					<option value="02" >February</option>
					<option value="03" >March</option>
					<option value="04" >April</option>
					<option value="05" >May</option>
					<option value="06" >June</option>
					<option value="07" >July</option>
					<option value="08" >August</option>
					<option value="09" >September</option>
					<option value="10" >October</option>
					<option value="11" >November</option>
					<option value="12" >December</option>
				</select>

				<select class="btn btn-secondary dropdown"  name="year">
					<option selected="" disabled="">Year</option>
					@for ($i = (date('Y')-18); $i>date('Y')-82; $i--)
                          <option>{{ $i }}</option>
                    @endfor
				</select>
			{!! $errors->first('day', '<span class="err p-0 alert alert-danger help-block">:message</span>') !!}
			{!! $errors->first('month', '<span class="err p-0 alert alert-danger help-block">:message</span>') !!}
            {!! $errors->first('year', '<span class="err p-0 alert alert-danger help-block">:message</span>') !!}
			</div>


			<div class="form-group mb-4 text-black" >
				<input  type="radio" name="gender" id="male" value="male">
				<label  for="male">Male</label>
				<input  type="radio" name="gender" id="female" value="female">
				<label  for="female">Female</label>
			{!! $errors->first('gender', '<span class="err p-0 alert alert-danger help-block">:message</span>') !!}
			</div>


			<div class="form-group">
				<button class="text-white btn btn-primary w-100 p-0" type="submit" name="sign_up">SIGN UP</button>
				<a href="{{ URL::to('/login') }}" class="btn btn-info p-0 mt-1 w-100">Login</a>
			</div>

		</form>
	</div>
</body>
</html>