@extends('doctype')
@extends('header')
@extends('footer')




@section('doctype')
   @parent
   <link rel="stylesheet" href="{{ asset('css/profile.css') }}">
   <link rel="stylesheet" href="{{ asset("css/page/friend.css") }}">
   <link rel="stylesheet" type="text/css" href="{{ asset("css/page/timeline.css") }}">
@endsection

@section('body')
   @parent
<div class="container-fluid row">
<div class="row col-md-10">
    <div class="col-sm-12">
        <div>
            <div class="content social-timeline">
                <div class="">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="social-wallpaper">
                                <img id='page_profile_cover' src="{{ asset($user['cover']) }}" alt="cover" width="100%" height="100%" style="object-fit: cover;">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-3 col-lg-4 col-md-4 col-xs-12">
                            <div class="social-timeline-left">
                                <div class="card">
                                    <div class="social-profile">
                                        <img  class="page_profile_photo img-fluid width-100" src="{{ asset($user['photo']) }}" alt="">
                                        <div class="profile-hvr m-t-15">
                                            <i class="icofont icofont-ui-edit p-r-10"></i>
                                            <i class="icofont icofont-ui-delete"></i>
                                        </div>
                                    </div>
                                    <div class="card-block social-follower">
                                        <h4>{{ $user['name']." ".$user["surname"] }}</h4>
                                        <div class="row follower-counter">
                                            <div class="col-4">
                                                <button class="btn btn-primary btn-icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="485">
                                                    <i class="fa fa-user"></i>
                                                </button>
                                            </div>
                                            <div class="col-4">
                                                <button class="btn btn-danger btn-icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="2k">
                                                    <i class="fa fa-thumbs-o-up"></i>
                                                </button>
                                            </div>
                                            <div class="col-4">
                                                <button class="btn btn-success btn-icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="90">
                                                    <i class="fa fa-eye">@if(!empty($guest))<sup style="color:red;font-size: 1.5em;">{{ $guest }}</sup>@endif</i>
                                                </button>
                                            </div>
                                        </div>
                               </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="card-header-text d-inline-block">Followers </h5>@if(isset($follows))<span class='follows-count label label-primary f-right'> {{  count($follows) }} </span>@endif
                                    </div>
                                    <div class="card-block user-box row">
                                        @if(!empty($follows))
                                          @foreach($follows as $item)
                                             <div class="col-4" data-user_id="{{ $item['id'] }}">
                                                <a href="{{ route('userprofile',['id'=>$item['id']]) }}" title="{{ $item['name']." ".$item['surname'] }}">
                                                    <img src="{{ asset($item['photo']) }}" alt="" width="50" height="50">
                                                </a>
                                            </div>
                                          @endforeach
                                      @endif
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="card-header-text d-inline-block">Friends</h5>

                                        <span class="label label-primary f-right">@if(isset($friends)){{ count($friends) }} @endif</span>
                                    </div>
                                    <div class="card-block friend-box row">
                                         @if(!empty($friends))
                                        @foreach($friends as $item)
                                        <div class="col-4" data-user_id="{{ $item['id'] }}">
                                        <a href="{{ route('userprofile',['id'=>$item['id']]) }}"
                                            title="{{ $item['name']." ".$item['surname'] }}">
                                            <img src="{{ asset($item['photo']) }}" alt="" 
                                            width="100%" height="50">  
                                           </a>
                                       </div>
                                        @endforeach
                                    @endif
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="col-xl-9 col-lg-8 col-md-8 col-xs-12 ">

                            <div class="card social-tabs">
                                <ul class="nav nav-tabs md-tabs tab-timeline flex-nowrap" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#timeline" role="tab">Timeline</a>
                                        <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#about" role="tab">About</a>
                                        <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link @if(isset($photo)) {{ 'active'  }} @endif " data-toggle="tab" href="#photos" role="tab" >Photos</a>
                                        <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#friends" role="tab">Friends</a>
                                        <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link guest" data-toggle="tab" href="#guest" role="tab">Guest @if(!empty($guest)) <span class="text-danger"> {{ $guest }} </span>@endif</a>
                                        <div class="slide"></div>
                                    </li>
                                </ul>
                            </div>

                            <div class="tab-content">

                                <div class="tab-pane active" id="timeline">
                                    @include('page.timeline')
                                </div>

                                <div class="tab-pane" id="about">
                                    @include('page.about')
                                </div>

                                <div class="tab-pane  @if(isset($photo)) {{ 'show active' }} @endif   " id="photos">
                                    <form action="{{ route('createphoto') }}" method="post" class="form-group" enctype="multipart/form-data">
                                         {{ csrf_field() }}
                                         <div class="form-group">
                                            <button class="btn btn-info">Add Photo</button>
                                            <input type="file"  id='foto' name="photos[]" multiple="multiple">
                                          </div>
                                          
                                      </form>
                                      <div class="photosline" id='page_photo'>
                                                @include('page.photos')
                                      </div> 
                                </div>

                                <div class="tab-pane" id="friends">
                                    @include('page.friend')
                                </div>
                                
                                <div class="tab-pane" id="guest">
                                    <div class="d-flex fleq-wrap w-100" id="myguest">
                                        
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<aside class="col-md-2">
            
</aside>
</div>
      
@endsection



@section('footer')
    @parent

    <script src="{{ asset("js/profile.js") }}"></script>
    <script src='{{ asset('js/page/about.js') }}'></script>
    <script src="{{ asset('js/page/timeline.js') }}"></script>
    
@endsection