
@section('body')
<body>
  <input type="hidden" name='user' data-user='{{ $user }}'>
  <header class="w-100">
    <nav class="navbar navbar-expand-md bg-info navbar-dark  fixed-top">
      <a class="navbar-brand " href="{{ route('home') }}">Profile</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse " id="collapsibleNavbar">
        <ul class="navbar-nav w-100 justify-content-end align-items-center">
          <li class="nav-item position-relative" id='users_search'>
            <input type="search" name="search"  placeholder="search" class="input_search form-control">
          </li>
          <li class="nav-item d-flex align-items-center text-white mr-2 ml-1">
            <img class='page_profile_photo' src="{{ asset($user['photo']) }}" style="width: 30px;height: 30px;border-radius: 50%;margin-right: 5px;" >
            <a href="{{ route('profile',['email' =>Session::get('email')]) }}" style='color:white;text-decoration: none;' >{!! $user['name'].' '.$user['surname'] !!}</a>
          </li>
          <li class="nav-item" id='user_plus'>
           <span class="nav-link plus_"><i class="fa fa-user-plus "></i><sup class="count_user_plus"></sup></span>
          </li>
          <li class="nav-item" id="bell_o">
           <span class="nav-link bell_"><i class="fa fa-bell-o "></i><sup class="count_bell_o"></sup></span>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{URL::to('/logout')}}"><i class="fa fa-sign-out"></i> Log OUT</a>
          </li>
      </ul>
    </div>
  </nav>
</header>  
<div class="chat_box">
</div>
@show
