@extends('doctype')
@extends('header')
@extends('footer')

@section('doctype')
   @parent
    <link rel="stylesheet" href="{{ asset("css/friend.css") }}">
    <link rel="stylesheet" href="{{ asset("css/page/friend.css") }}">
    <link rel="stylesheet" href="{{ asset("css/page/timeline.css") }}">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
@endsection

@section('body')
   @parent
   

<div class="container-fluid row mt-2">
<div class="row col-md-10">
    <div class="col-sm-12">
        <div>
            <div class="content social-timeline">
                <div class="">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="social-wallpaper">
                                <img src="http://hubancreative.com/projects/templates/coco/corporate/images/stock/1epgUO0.jpg" alt="" width="100%" height="100%">
                            </div>
                            <div class="timeline-btn">
                                @if($follow !== false)
                                   <button data-user_id="{{ $friend['id'] }}" class="mb-2 btn btn-danger waves-effect waves-light m-r-10 unfollows">UnFollows</button>
                                @else
                                    <button data-user_id="{{ $friend['id'] }}" class="mb-2 btn btn-primary waves-effect waves-light m-r-10 follows">Follows</button>
                                @endif
                                   <button id="sendmessge"class="mb-2 btn btn-primary waves-effect waves-light">Send Message</button>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-3 col-lg-4 col-md-4 col-xs-12">
                            <div class="social-timeline-left">
                                <div class="card">
                                    <div class="social-profile">
                                        <img class="img-fluid width-100" src="{{ asset($friend['photo']) }}" alt="">
                                        <div class="profile-hvr m-t-15">
                                            <i class="icofont icofont-ui-edit p-r-10"></i>
                                            <i class="icofont icofont-ui-delete"></i>
                                        </div>
                                    </div>
                                    <div class="card-block social-follower">
                                        <h4>{{ $friend['name']." ".$friend['surname'] }}</h4>
                                        <h5>Softwear Engineer</h5>
                                        
                                        <div class="friend-btn">
                                            <button data-friend_id="{{ $friend['id'] }}" type="button" class="btn btn-outline-danger waves-effect btn-block unfriend">UnFriend</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="card-header-text d-inline-block ">Followers </h5>@if(isset($follows))<span class='follows-count label label-primary f-right'> {{ count($follows) }} @endif</span>
                                    </div>
                                    <div class="card-block user-box row">
                                      @if(!empty($follows))
                                          @foreach($follows as $item)
                                             <div class="col-4" data-user_id="{{ $item['id'] }}">
                                                <a href="{{ route('userprofile',['id'=>$item['id']]) }}" title="{{ $item['name']." ".$item['surname'] }}">
                                                    <img src="{{ asset($item['photo']) }}" alt="" width="50" height="50">
                                                </a>
                                            </div>
                                          @endforeach
                                      @endif
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="card-header-text d-inline-block">Friends</h5>

                                        <span class="label label-primary f-right">@if(isset($friends)){{ count($friends) }} @endif </span>

                                    </div>
                                    <div class="card-block friend-box row">
                                     @if(!empty($friends))
                                        @foreach($friends as $item)
                                        <div class="col-4" data-user_id="{{ $item['id'] }}">
                                        <a href="{{ route('userprofile',['id'=>$item['id']]) }}"
                                            title="{{ $item['name']." ".$item['surname'] }}">
                                            <img src="{{ asset($item['photo']) }}" alt="" 
                                            width="100%" height="50">  
                                           </a>
                                       </div>
                                        @endforeach
                                    @endif
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="col-xl-9 col-lg-8 col-md-8 col-xs-12 ">

                            <div class="card social-tabs">
                                <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#timeline" role="tab">Timeline</a>
                                        <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#about" role="tab">About</a>
                                        <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#photos" role="tab">Photos</a>
                                        <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#friends" role="tab">Friends</a>
                                        <div class="slide"></div>
                                    </li>
                                </ul>
                            </div>

                            <div class="tab-content">
                              
                              {{-- start timeline --}}
                                <div class="tab-pane active" id="timeline">

                                    {{-- @foreach($timeline as $item) --}}
                                     @include('page.timeline');
                                    {{-- @endforeach --}}
            
                                </div>
                                {{-- end timeline     --}}

                                <div class="tab-pane" id="about">
                                      @include('page.about')

                                </div>

                                <div class="tab-pane" id="photos">
                                       <div class="photosline" id='page_photo'>
                                                @include('page.photos')
                                      </div> 
                                </div>

                                <div class="tab-pane" id="friends">
                                      @include('page.friend')
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<aside class="col-md-2">
            
</aside>

@endsection











@section('footer')
   @parent
   <script src="{{ asset('js/friend.js') }}"></script>
   <script src="{{ asset('js/page/timeline.js') }}"></script>
   <script type="text/javascript" src="{{ asset("js/page/friend.js") }}"></script>
@endsection