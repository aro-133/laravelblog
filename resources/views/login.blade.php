@extends('doctype')



@section('doctype')
    @parent
<link rel="stylesheet" href="{{  asset('css/login.css')}}">
@endsection
</head>
<body>
    <div class="container">
        <div class="card card-container">
            <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
            <p id="profile-name" class="profile-name-card"></p>
            <form class="form-signin" action="{{ URL::to('login/post') }}" method="post">
                
                {{ csrf_field() }}

                {!! $errors->first('verify', '<p class="p-0 text-center alert alert-danger help-block">:message</p>') !!}
                <div class="form-group">
                <input type="email" id="inputEmail"  name='email' class="form-control"placeholder="Email address" value="" required autofocus>
                </div>
                <div class="form-group">
                <span id="reauth-email" class="reauth-email"></span>
                <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" value="" required>
                </div>
                <div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox" name="checked" value="remembermy"> Remember me
                    </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit" name="login">Login</button>
                <a href="{{ URL::to('/') }}" class="btn btn-primary w-100">Sign Up</a>
            </form><!-- /form -->
            <a href="{{ URL::to('/login/forgotpassword') }}" class="forgot-password">
                Forgot the password?
            </a>
        </div><!-- /card-container -->
    </div><!-- /container -->
</body>
</html>