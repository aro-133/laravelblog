@extends('doctype')
@extends('header')
@extends('footer')

@section('doctype')
     @parent
     <link rel="stylesheet" type="text/css" href="{{ asset('css/home.css') }}">
     <link rel="stylesheet" href="{{ asset('css/share.css') }}">
@endsection

@section('body')
     @parent
      <div class="row w-100">
      	<section class="col-md-3 p-0">

	<div class="d-flex justify-content-center align-items-center">
		<div class="image">
		    <img src="{{ $user['photo']}}">           
	   </div>
	</div>
	<div class="h-50">
		<table>
					<tr>
						<td><span>Name:</span></td>
						<td id='tdname'>{!! $user['name'] !!}</td>
					</tr>
					<tr>
						<td><span>Surname:</span></td>
						<td id='tdsurname'>{!! $user['surname'] !!}</td>
					</tr>
					<tr>
						<td><span>Birthday:</span></td>
						<td id='tdbirth'>{!! $user['birthday'] !!}</td>
					</tr>
					<tr>
						<td><span>Gender:</span></td>
						<td>{!! $user['gender'] !!}</td>
					</tr>
					<tr>
						<td><span>Email:</span></td>
						<td><a href="www.mail.com" id='tdemail' class="text-white">{{ $user['email'] }}</a></td>
					</tr>
				</table>
	</div>
		
		</section>
		<article class="col-md-6">
    						<div class="widget-area no-padding blank">
								<div class="status-upload">
									<form action="{{URL::to("home/share/post")}}" method="post" enctype="multipart/form-data" id="share_form">
										 @csrf
										<textarea class="form-control" rows="5" id="comment" name="share_text" placeholder="whats on your mind"></textarea>
                                        <div id='file'></div>       
										<ul id="addfile">
											<li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Audio"><i class="fa fa-music"></i></a>
											<input type="file" accept="audio/*" name="share_audio"></li>
											<li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Video"><i class="fa fa-video-camera"></i></a>
                                            <input type="file" accept="video/*" name="share_video" id='share_video' data-toggle="tooltip" data-placement="bottom" data-original-title="Video">
											</li>
											<li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Sound Record"><i class="fa fa-microphone"></i></a></li>
											<li ><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Picture">
											<i class="fa fa-picture-o"></i></a>
											<input type="file" name="share_photo" multiple id='share_file'data-toggle="tooltip" data-placement="bottom" data-original-title="Picture">
											</li>
										</ul>
										<button name="share_post" type="submit" class="btn btn-success green"><i class="fa fa-share"></i> Share</button>
									</form>
								</div><!-- Status Upload  -->
							</div><!-- Widget Area -->
							<div id="status">.</div>	
		</article>
		<aside class="col-md-3">
			
		</aside>




      </div>
@endsection


@section('footer')
    @parent  
<script src="{{ asset('js/profile.js') }}"></script>
<script src="{{ asset("js/share.js") }}"></script>

@endsection

