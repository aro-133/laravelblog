<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>  
@yield('footer');
<script src="{{ asset('js/page/photo.js') }}"></script>
<script src="{{ asset('js/header.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/chat.js') }}"></script>
{{-- <script src="/socket.io/socket.io.js"></script> --}}
</head>
</html>