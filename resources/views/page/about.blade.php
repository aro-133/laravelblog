        
    <?php if(!empty($friend)) 
         {
            $data = $friend;
            $isuserpage = false;
         }
         else
         {
            $data = $user;
            $isuserpage = true;
         }
     ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div>
                    <h5 class="card-header-text">Basic Information</h5>
                    @if($isuserpage)
                    <button id="edit-about" type="button" class="btn btn-primary waves-effect waves-light f-right">
                        Edit
                    </button>
                    @endif
                    </div>
                    <div>
                    <h5 class="card-header-text">Reset Password</h5>
                    @if($isuserpage)
                    <button id="edit-reset" type="button" class="btn btn-outline-danger waves-effect waves-light f-right"><i class="fa fa-cog "></i></button>
                    @endif
                    </div>
                </div>
                <div class="card-block row">
                    <div id="view-info" class="row col-12 col-sm-6">
                        <div class="col-12">
                            <form>
                                <table class="table table-responsive m-b-0">
                                    <tbody>
                                        <tr>
                                            <th class="social-label b-none p-t-0">Name
                                            </th>
                                            <td class="social-user-name b-none p-t-0 text-muted">{{ $data['name'] }}</td>
                                        </tr>
                                        <tr>
                                            <th class="social-label b-none p-t-0">Surname
                                            </th>
                                            <td class="social-user-surname b-none p-t-0 text-muted">{{ $data['surname'] }}</td>
                                        </tr>
                                        <tr>
                                            <th class="social-label b-none p-t-0">Email
                                            </th>
                                            <td class="social-user-email b-none p-t-0 text-muted">{{ $data['email'] }}</td>
                                        </tr>
                                        <tr>
                                            <th class="social-label b-none">Gender</th>
                                            <td class="social-user-gender b-none text-muted">{{ $data['gender'] }}</td>
                                        </tr>
                                        <tr>
                                            <th class="social-label b-none">Birth Day</th>
                                            <td class="social-user-age b-none text-muted">{{ $data['birthday'] }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>

                    {{-- edit users  --}}
                 
                    <div id="edit-info" class="col-12 col-sm-6" style="display: none;">
                        <div class="w-100">
                            
                                <div class="input-group mb-2">
                                    <input type="text" name='name' class="form-control" placeholder="Name">
                                </div>
                                <div class="input-group mb-2">
                                    <input type="text" name='surname' class="form-control" placeholder="Surname">
                                </div>
                                <div class="input-group mb-2">
                                    <input type="text" name='email' class="form-control" placeholder="email">
                                </div>
                                <div class="input-group mb-2">
                                    <div class="form-radio">
                                        <div class="form-radio">
                                            <label class="md-check p-0">Gender</label>
                                            <div class="radio radio-inline">
                                                <label>
                                                    <input type="radio" name="radio" value="male">
                                                    <i class="helper"></i>Male
                                                </label>
                                                <label>
                                                    <input type="radio" name="radio" value="female">
                                                    <i class="helper"></i>Female
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-2 ">
                                 <input type="date" name='birthday' class="form-control">
                                </div>
                    
                                <div class="text-right m-t-20">
                                    <button id="edit-save" class="btn btn-primary waves-effect waves-light m-r-20">Save</button>
                                    <button id="edit-cancel" class="btn btn-default waves-effect waves-light">Cancel</button>
                                </div>
                        </div>
                    </div>


                    {{-- resset password --}}
                <div id="edit-reset-password" class="col-12 col-sm-6" style="display: none;">
                        <div class="w-100">
                            
                                <div class="input-group mb-2">
                                    <input class='reset-oldpassword form-control' type="password" name='oldpassword' placeholder="oldpassword">
                                </div>
                                <div class="input-group mb-2">
                                    <input class="reset-password form-control" type="password" name='password' placeholder="password">
                                </div>
                                <div class="input-group mb-2">
                                    <input class="reset-password_confirmation form-control" type="password" name='password_confirmation' placeholder="password_confirmation">
                                </div>
                                <div class="text-right m-t-20">
                                    <button id="edit-reset-save" class="btn btn-primary waves-effect waves-light m-r-20">Save</button>
                                    <button id="edit-reset-cancel" class="btn btn-default waves-effect waves-light">Cancel</button>
                                </div>
                        </div>
                    </div>
                
{{-- end reset password --}}

                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-header-text">Contact Information</h5>
                    @if($isuserpage)
                    <button id="edit-contact" type="button" class="btn btn-primary waves-effect waves-light f-right">
                        Edit</i>
                    </button>
                    @endif
                </div>
                <div class="card-block">
                    <div id="contact-info" class="row">
                        <div class="col-lg-6 col-md-12">
                            <table class="table table-responsive m-b-0">
                                <tbody>
                                    <tr>
                                        <th class="social-label b-none p-t-0">Mobile Number</th>
                                        <td class="social-user-mobile b-none p-t-0 text-muted">eg. (0123) - 4567891</td>
                                    </tr>
                                    <tr>
                                        <th class="social-label b-none">Email Address</th>
                                        <td class="social-user-gmail b-none text-muted">test@gmail.com</td>
                                    </tr>
                                    <tr>
                                        <th class="social-label b-none">Twitter</th>
                                        <td class="social-user-odherpage b-none text-muted">@phonixcoded</td>
                                    </tr>
                                    <tr>
                                        <th class="social-label b-none p-b-0">Skype</th>
                                        <td class="social-user-skype b-none p-b-0 text-muted">@phonixcoded demo</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="edit-contact-info" class="row" style="display: none;">
                        <div class="col-6">
                            <form>
                                <div class="input-group mb-2">
                                    <input type="text" name = "mobile_number" class="form-control" placeholder="Mobile number">
                                </div>
                                <div class="input-group mb-2">
                                    <input type="text" name = "email_addres" class="form-control" placeholder="Email address">
                                </div>
                                <div class="input-group mb-2">
                                    <input type="text" name = 'twitter_id' class="form-control" placeholder="Twitter id">
                                </div>
                                <div class="input-group mb-2">
                                    <input type="text" name="skype_id" class="form-control" placeholder="Skype id">
                                </div>
                                <div class="d-flex justify-content-between">
                                    <button id="contact-save" class="btn btn-primary waves-effect waves-light m-r-20">Save</button>
                                    <button id="contact-cancel" class="btn btn-default waves-effect waves-light">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-header-text">Work</h5>
                    @if($isuserpage)
                    <button id="edit-work" type="button" class="btn btn-primary waves-effect waves-light f-right">
                        Edit</i>
                    </button>
                    @endif
                </div>
                <div class="card-block">
                    <div id="work-info" class="row">
                        <div class="col-lg-6 col-md-12">
                            <table class="table table-responsive m-b-0">
                                <tbody>
                                    <tr>
                                        <th class="social-label b-none p-t-0">Occupation &nbsp; &nbsp; &nbsp;
                                        </th>
                                        <td class="social-user-work b-none p-t-0 text-muted">Developer</td>
                                    </tr>
                                    <tr>
                                        <th class="social-label b-none">Skills</th>
                                        <td class="social-user-language b-none text-muted">C#, Javascript, Anguler</td>
                                    </tr>
                                    <tr>
                                        <th class="social-label b-none">Jobs</th>
                                        <td class="social-user-jobs b-none p-b-0 text-muted">#</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {{-- start edit work --}}
                    <div id="edit-contact-work" class="row" style="display:none;">
                        <div class="col-6">
                            <form>
                                <div class="input-group mb-2">
                                    <input type="text" class="form-control" name="occupatio" placeholder="occupation">
                                </div>
                                <div class="input-group mb-2">
                                    <input type="text" class="form-control" name="skills" placeholder="skills">
                                </div>
                                <div class="input-group mb-2">
                                    <input type="text" class="form-control" name="jobs"
                                    placeholder="jobs">
                                </div>
                                <div class="d-flex justify-content-between">
                                    <button id="about-work-save" class="btn btn-primary waves-effect waves-light m-r-20">Save</button>
                                    <button id="about-work-cancel" class="btn btn-default waves-effect waves-light">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div> {{-- end edit work --}}

                </div>
            </div>
        </div>
    </div>