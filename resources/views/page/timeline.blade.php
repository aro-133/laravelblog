@if(!empty($timeline))
@foreach($timeline as $item)
            @if($item['post_hide'] == 0)
                <div class="user_post card gedf-card timeline" data-share_id='{{ $item['id'] }}'>
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="mr-2">
                                    <img class="rounded-circle" width="45" src="{{ asset($item['avatar']) }}" alt="">
                                </div>
                                <div class="ml-2">
                                    <div class="h7 text-muted"><a href="{{ route('userprofile',['id'=>$item['user_id']]) }}">
                                        {{ $item['name']." ".$item['surname'] }}
                                    </a></div>
                                </div>
                            </div>
                            <div>
                                <div class="dropdown">
                                    <button class="btn btn-link dropdown-toggle" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                                        <div class="h6 dropdown-header">Configuration</div>
                                        @if($user['id']==$item['user_id'])
                                        <p style='cursor:pointer;'class="dropdown-item post-delete">Delete</p>
                                        @endif
                                        <p style='cursor:pointer;'class="dropdown-item post-hide">Hide</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-body">
                        <div class="text-muted h7 mb-2"> <i class="fa fa-clock-o"></i>{{ $item['time'] }}</div>

                        @if($item['text'])
                            <h5 class="card-title text-primary">{{ $item['text'] }}</h5>
                        @endif

                        @if($item['photo'])
                            <img width = '100%' height = '400px' src="{{ $item['photo'] }}">
                        @endif

                        @if($item['video'])
                            <video width="100%" height="300px" controls>
                                  <source src="{{ $item['video'] }}">
                            </video>
                        @endif
                      
                       @if($item['audio'])
                            <audio width="100%" controls>+
                                  <source src="{{ $item['audio'] }}">
                            </audio>
                       @endif

                       @if($item['youtube'])
                            <iframe width="100%" height="400" src='{{ $item["youtube"] }}'
                                    frameborder="0" allow="autoplay; encrypted-media" allowfullscreen>
                            </iframe>
                      @endif
                    </div>
                    
                    <div class="card-footer d-flex justify-content-around">

                    @if( $item['testlikeshare'] == '1' )
                    <div class="d-inline-flex align-items-center position-relative">
                       <span class="mr-2"><i class="fa fa-thumbs-o-up text-primary"></i></span>
                       <button class="btn btn-outline-danger dislike_post">
                                <span class="like-show like_count{{ $item["id"]}}">{{ $item["like_count"] }}</span>
                               <i class="fa fa-gittip "></i> DisLike
                        </button>
                    </div>
                    @else
                    <div class="d-inline-flex align-items-center position-relative">
                        <span class="mr-2"><i class="fa fa-thumbs-o-up text-primary"></i></span>
                        <button class="btn btn-outline-primary like_post">
                               <span class="like-show like_count{{ $item["id"]}}">{{ $item["like_count"]}}</span>
                               <i class="fa fa-gittip "></i> Like
                        </button>
                    </div>
                    @endif
                    <div class="d-inline-flex align-items-center position-relative">
                        <span class="mr-2"><i class="fa fa-comment text-primary"></i></span>
                        <button class="btn btn-outline-info card-link comment_post">
                            <span class="comment_count{{$item['id'] }}">{{ $item["comment_count"] }}</span>
                            Comment
                        </button>
                     </div>
                        <button class="btn btn-outline-info card-link"><i class="fa fa-mail-forward"></i> Share</button>
                    </div>
                    <div class='card-body'>
                        <input type='text' class='form-control commit p-4'>
                    </div>
                    <div class="card-body">
        <div  class="alert alert-dark share_comments w-100 p-1 comments{{ $item['id']}}"></div>
    </div>
</div>
@endif
@endforeach
@endif