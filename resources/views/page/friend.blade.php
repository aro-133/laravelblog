 @if(!empty($friends))
 @foreach($friends as $item)
 <div class="row" id='friends-friend-box'>
  <div class="col-10 m-auto">
    <div class="people-nearby">
      <div class="nearby-user">
        <div class="row friends-friend-list" data-friend_id="{{ $item['id'] }}" >
          <div class="col-md-2 col-sm-2">
            <img src="{{ asset($item['photo']) }}" alt="user" class="profile-photo-lg">
          </div>
          <div class="col-md-7 col-sm-7">
            <h5><a href="{{ route("userprofile",['id'=>$item['id']]) }}" class="profile-link">{{ $item['name']." ".$item['surname'] }}</a></h5>
            <p id="online-text" class='online-text{{ $item['id'] }}'><i></i></p>
          </div>
          <div class="col-sm-3 friends-friend-btn">

            @if($item['status'] == 'acceptrequest')

            <div class="btn-group">
               <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Request</button>
               <div class="dropdown-menu bg-info">
                 <button class="btn btn-primary dropdown-item friend-accept-request" >Accept</button>
                 <button class="btn btn-danger dropdown-item friend-cancel-request" >Cancel</button>
               </div>
            </div>

            @endif
            @if($item['status'] == 'cancelrequest')

            <button class="btn btn-warning pull-right friend-cancel-request">Cancel Request</button>


            @endif
            @if($item['status'] == 'friend')

            <button class="btn btn-danger pull-right friend-unfriend">UnFriend</button>


            @endif
            @if($item['status'] == 'unfriend' )

            <button class="btn btn-info pull-right friend-send-request">Add Friend</button>

            @endif

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endforeach
@endif 