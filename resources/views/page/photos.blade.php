

<!------ Include the above in your HEAD tag ---------->
 @if(isset($gallery))
    @foreach($gallery as $item =>$val)
        <div class="profile-page-photo box " data-photo_id="{{ $val['id'] }}">
            <img data-picture_id="{{ $val['id'] }}" class="mySlides" id="{{ $item }}" src="{{ $val['photo'] }}" alt="{{ $val['photo'] }}" width="100%" height="100%">
        </div>
    @endforeach
@endif