<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\User;
use App\Friends;

Route::get("/",function(){
	return view("register");
});
Route::post("/post","Auth\RegisterController@validator");
Route::get("login",function(){
	return view("login");
})->name('login');
Route::get("/verify/{id}","Auth\RegisterController@verify");
Route::post("/login/post",'Auth\LoginController@authenticate');

Route::get("/logout","Auth\LoginController@logout");

//forgot password
Route::get('/login/forgotpassword',function(){
	return view('forgot');
});

Route::post('login/forgotpasswordd/code','Auth\ForgotPasswordController@code');
Route::post('login/forgotpasswordd/forgot','Auth\ForgotPasswordController@forgotpassword');
Route::post('login/forgotpasswordd/email','Auth\ForgotPasswordController@sendemail');
//end forgotpassword

Route::group(['prefix'=>'home','middleware'=>['handle','web','auth']],function(){
	Route::get("/","UserController@home")->name('home');
	Route::post("/search","UserController@searchusers");
	Route::post("/send/post","RequestController@sendrequest");
	Route::post("/cancel/post","RequestController@cancelrequest");
	Route::post("/request/post","RequestController@viewrequest");
	Route::post("/acceptrequest/post","RequestController@acceptrequest");
	Route::post("/cancelreq/post","RequestController@cancelreq");
	Route::post("/myfriends/post","UserController@myfriends");
    Route::get('/user/{id}','UserController@userProfile')->name('userprofile');
    Route::get('/profile/{email}',"UserController@myProfile")->name('profile');
    Route::post("/share/post","ShareController@share");
    Route::post("/getpost","ShareController@getpost");
    Route::post("/likeshare","ShareController@likeshare");
    Route::post("/dislikeshare","ShareController@dislikeshare");
    Route::post("/testlikeshare","ShareController@testlikeshare");
    Route::post("/commentshare","ShareController@commentspost");
    Route::post('/getallcomments',"ShareController@getallcomments");
    Route::post("/usersonline","OnlineController@UsersOnline");
    Route::post("/chengepassword","PageAboutController@ChangePassword");
    Route::post("/page/getallphoto","UsersPhotosController@GetAllPhoto");
    Route::post("/page/likephoto","UsersPhotosController@LikePhoto");
    Route::post("/page/dislikephoto","UsersPhotosController@DislikePhoto");
    Route::post("/page/commentphoto","UsersPhotosController@CommentPhoto");
    Route::post("/setmessage","ChatController@SetMessage");
    Route::post("/getmessage","ChatController@GetMessage");
    Route::post("/searchmessage","ChatController@SearchMessage");
});

   

Route::group(['prefix'=>'home/user','middleware'=>['handle','web','auth']],function(){
    Route::post("page/followspage","FriendController@followsUserPage");
    Route::post("page/unfollowspage","FriendController@UnFollowsPage");
    Route::post('/friends/cancel',"PageController@CancelRequest");
    Route::post('/friends/accept',"PageController@AcceptRequest");
    Route::post('/friends/unfriend',"PageController@UnFriend");
    Route::post('/friends/sendrequest',"PageController@SendRequest");
});
Route::group(['prefix'=>'home/profile','middleware'=>['handle','auth']],function(){
    Route::post("/create","UsersPhotosController@create")->name('createphoto');
    Route::get('/create/{photo}',"UsersPhotosController@show") -> name('showphoto');  
    Route::post('/about/editinfo',"PageAboutController@update");
    Route::post("/timeline/delete","PageController@DeletePost");
    Route::post("/timeline/hide","PageController@HidePost");
    Route::post("/timeline/showlikes","PageController@ShowLikes");
    Route::post('/friends/unfriend',"PageController@UnFriend");
    Route::post("/myguest","PageController@Guest");
    Route::post("/page/chengephoto","UsersPhotosController@updateProfile");
    Route::post("/page/chengecover","UsersPhotosController@updateCover");
    Route::post("/page/deletephoto","UsersPhotosController@destroy");
});
