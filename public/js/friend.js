$(document).ready(function(){
$.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
$(document).on('click','.follows',function(){
    var user_id = $(this).data('user_id'),
        _this = this;
    $.ajax({
    	type:'post',
    	url:'page/followspage',
    	data:{user_id:user_id},
    	success:function(data){
        $(".follows-count,.user-box").empty();
        $(".follows-count").html(" "+data.length);
        data.forEach((item)=>{
    		var div = $("<div class='col-4'></div>");
    		         div.data("user_id",item['id']);
    		         div.append('<a title="'+item['name']+" "+item['surname']+'"  href ="home/user/'+item['id']+'" >'+
                               '<img src="'+item['photo'] +'" width = "50" height="50">'+
                           '</a>');
    		$(".user-box").append(div);
      })
       $(_this).removeClass('follows btn-primary').addClass('unfollows btn-danger').html('UnFollows'); 

    }
 })

})

$(document).on('click','.unfollows',function(){
	var user_id = $(this).data('user_id'),
	    _this = this;
	    $.ajax({
	    	type:'post',
	    	url:'page/unfollowspage',
	    	data:{user_id:user_id},
	    	success:function(data){
          console.log(data);
        $(".follows-count,.user-box").empty();
	     if(data){
        $(".follows-count").html(" "+data.length);
        data.forEach((item)=>{
        var div = $("<div class='col-4'></div>");
                 div.data("user_id",item['id']);
                 div.append('<a title="'+item['name']+" "+item['surname']+'"  href ="home/user/'+item['id']+'" >'+
                               '<img src="'+item['photo'] +'" width = "50" height="50">'+
                           '</a>');
        $(".user-box").append(div);
        })      
      }
      $(_this).removeClass('unfollows btn-danger').addClass('follows btn-primary').html('Follows');
	    	
	    	}
	 })
})
 $(document).on("click",'.unfriend',function(){
           var user_id = $(this).data('friend_id'),
               _this=this;
               $.ajax({
                type:'post',
                url:'friends/unfriend',
                data:{user_id:user_id},
                success:function(data){

                  $(_this).removeClass('btn-outline-danger unfriend').addClass('btn-outline-primary sendrequest').html('Add Friends');
                }
      })
   })
 $(document).on("click",'.sendrequest',function(){
           var user_id = $(this).data('friend_id'), 
              _this=this;
               $.ajax({
                type:'post',
                url:'friends/sendrequest',
                data:{user_id:user_id},
                success:function(data){

                 $(_this).removeClass('btn-outline-primary sendrequest').addClass('btn-outline-warning cancelrequest').html('Cancel Request');

                }
      })
   })
  $(document).on("click",'.cancelrequest',function(){
            var user_id = $(this).data('friend_id'),
               _this=this;
               console.log(user_id);
               $.ajax({
                type:'post',
                url:'friends/cancel',
                data:{user_id:user_id},
                success:function(data){

                  $(_this).removeClass('btn-outline-warning cancelrequest').addClass('btn-outline-primary sendrequest').html('Add Friends');
                }
      })
   })
});