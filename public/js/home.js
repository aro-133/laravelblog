MyFriends();
function MyFriends(){
  $.ajax({
        type:'post',
        url:'/home/myfriends/post',
        success:function(data){
           $('aside').empty();
            data.forEach((item) => {
            var ul = $('<ul class="list-group friend"></ul>');
                ul.data("friend_id", item['id']);
                ul.data("user", item);
            var li = $('<li class=" list-group-item list-group-flush list-group-item-action d-flex justify-content-between align-items-center"></li>');
            var svg = $('<svg width="16" height="16" class="OnOff">'+
                             '<circle cx="8" cy="8" r="5" stroke="black" stroke-width="1" fill="transparent" />'+
                         '</svg>');
            var a = $('<a href="/home/user/'+item['id']+'" class="list-group-item"></a>');
                a.append("<img src='" + item['photo'] + "' width = '30'height = '30' style='border-radius:50%' >");
                a.append("<span> " + item['name'] + " " + item['surname'] + "</span>");
                a.prepend(svg);
                li.append(a);
                li.append('<span class="open_message badge badge-success badge-pill">'+
                            '<i class="fa fa-envelope-o"></i>'+
                            '<sup style="font-size:1.6em;color:red"></sup>'+
                          '</span>');
                ul.append(li);
            $("aside").append(ul);
          })
        
        }
  })
}


