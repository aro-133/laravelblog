
$(document).ready(function(){
$.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
var chatbox = [];
$(document).on("click",".open_message",function(){
    var user = $(this).parents('ul').data('user');
    $(this).find("sup").html("");
    var chat = $('<div id="frame'+user['id']+'" class="frame">'+
                     '<div class="d-flex justify-content-between align-items-center bg-dark p-1">'+
                         '<div class="w-75">'+
                              "<img src='"+user['photo']+"' width = '25' height = '25'>"+
                              "<a href='/home/user/"+user['id']+"' style='color:white'> "+user['name']+" "+user['surname']+"</a>"+
                          '</div>'+
                          '<div class="w-25 d-flex justify-content-around align-items-center">'+
                             '<span class="minus_message badge badge-success "><i class="fa fa-minus"></i></span>'+
                             '​<span class="close_message badge badge-danger ">close</span>'+
                          "</div>"+
                     '</div>'+
                       '<ul class="chatmain"></ul>'+
                    '<div class="inputmain">'+
                       '<div class="msj-rta macro" style="margin:auto">'+                        
                         '<div class="text text-r" style="background:whitesmoke !important">'+
                           '<input class="mytext" placeholder="Type a message"/>'+  
                        '</div>'+ 
                       '</div>'+
                   '</div>'+
                 '</div>');
    chat.data("user",user);
    if(chatbox.indexOf(user['id']) == -1)
    {
           if(chatbox.length<3)
           {
              chatbox.push(user['id']);
            }
           else
            {
              $("#frame"+chatbox.shift()).remove();
              chatbox.push(user['id']);
            }
        $(".chat_box").append(chat);
    }
    else
    {
        $("#frame"+user['id']).focus();
    }

    GetMessage(user['id']);
})
$(document).on("click",".close_message",function(){
     var user = $(this).parents('.frame').data('user');
         $(this).parents('.frame').remove();
         chatbox.splice(chatbox.indexOf(user['id']),1);
})
// var socket = new WebSocket('ws://aroblog.am:8090');

var me = {};
var you = {};

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}            

//-- No use time. It is a javaScript effect.
function insertChat(who, data,id, time = 0){
    var control = "";
    if (who == "me"){
        
        control = '<li style="width:100%;height:auto">' +
                        '<div class="msj macro">' +
                        '<div class="avatar"><img class="img-circle" style="width:45px;height:32px;" src="'+ data['photo'] +'" /></div>' +
                            '<div class="text text-l">' +
                                '<p>'+ data['message'] +'</p>' +
                                '<p><small>'+data['time']+'</small></p>' +
                            '</div>' +
                        '</div>' +
                    '</li>';                    
    }else{
        control = '<li style="width:100%;">' +
                        '<div class="msj-rta macro">' +
                            '<div class="text text-r">' +
                                '<p>'+data['message']+'</p>' +
                                '<p><small>'+data['time']+'</small></p>' +
                            '</div>' +
                            '<div class="avatar" style="padding:0px 0px 0px 10px !important">'+
                               '<img class="img-circle" style="width:45px;height:32px;" src="'+data['photo']+'" />'+
                            '</div>' + 
                        '</div>'+                               
                  '</li>'
    }
    
    $("#frame"+id+" ul").append(control).scrollTop($("#frame"+id+" ul").prop('scrollHeight'));

}

    $(document).on("change",".frame .mytext",function(){

             var user = $(this).parents(".frame").data("user");
                 text = $(this).val(),
                _this = this;
            if (text !== ""){
                    $.ajax({
                        type:'post',
                        url:'/home/setmessage',
                        data:{user_id:user['id'],message:text},
                        success:function(data){
                            insertChat("me",data,user['id']);              
                            $(_this).val('');              
                        }
                    })
                
            }
        
    });

// socket.onmessage = function(msg) {
//     console.log(msg);
// }

//-- Clear Chat
function GetMessage(user_id)
{   
    $.ajax({
        type:'post',
        url:'/home/getmessage',
        data:{user_id:user_id},
        success:function(data){
          if(data)
          { 
            $("#frame"+user_id+" ul").empty();
            data.forEach((item)=>{
                if(item['user1_id'] == user_id )
                {
                    insertChat("you",item,user_id);
                }
                else
                {
                    insertChat('me',item,user_id);
                }
            })
          }
        }
    })
}

function count(){
    $.ajax({
        type:'post',
        url:'/home/searchmessage',
        data:{},
        success:function(data){
            if(data)
            {  
                console.log(data);
                data.forEach((item)=>{
                    $("aside .friend").each(function(){
                        $user = $(this).data('user')
                        if($user['id'] == item['user1_id'])
                        {
                           if(chatbox.indexOf(item['user1_id'])!=-1)
                           {
                              GetMessage(item['user1_id']);
                           }
                           else
                           {
                              $(this).find('sup').html(item['count']);
                           }
                        }
                        
                    })
                })
            }
        }

});
}
setInterval(()=>{
    count();
},1000);

$(document).on("click",'.minus_message',function(){
    $(this).parents(".frame").find('.inputmain').toggle();
    if($(this).parents(".frame").hasClass("chatminusframe"))
    {
        $(this).parents('.frame').removeClass("chatminusframe");
    }
    else
    {
        $(this).parents('.frame').addClass("chatminusframe");
    }

})
//-- Print Messages
//-- NOTE: No use time on insertChat.
})