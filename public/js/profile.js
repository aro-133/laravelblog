$(document).ready(function(){
 $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  
  $(document).on('change','#foto',function(){
  	$(this).parents("form").submit();
  })

  $(document).on("click",'.friend-unfriend',function(){
           var user_id = $(this).parents(".friends-friend-list").data("friend_id"),
               _this=this;
               $.ajax({
                type:'post',
                url:'friends/unfriend',
                data:{user_id:user_id},
                success:function(data){

                  $(_this).parents("#friends-friend-btn").remove();

                }
      })
   })
  $(document).on("click",".guest",function(){

  	$.ajax({
  		type:'post',
  		url:'myguest',
  		data:{},
  		success:function(data){
  			$("#myguest,.fa-eye > sup,.guest>span").empty();
  			data.forEach((item)=>{
  				var div = $("<div class='w-25 text-center alert alert-info p-0 mr-2'></div>");
  				    div.data("user_id",item['id']);
  				    div.append('<img width="100"  height="100" src="'+item['photo']+'" alt="user" class="profile-photo-lg">')
                               
  				    if(item['status']=='0')
  				       var col = 'text-danger';
  				    else
  				       var col = 'text-primary';
  				    div.append('<h5><a style="font-size:0.7em" href="/home/user/'+item['id']+'" class="profile-link '+col+'">'+item['name']+" "+item['surname']+'</a></h5>'+
                                "<p class='online-text"+item['id']+"'><i></i></p>"+
                                '<p style="font-size:0.7em" class="text-muted">'+item['date']+'</p>');
  				    $('#guest #myguest').prepend(div);
  			})
  		}

  	})
  })


});