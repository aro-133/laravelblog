$(document).ready(function() {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });


  $(document).on("input", ".input_search", function() {
    $("#search_base").remove();
    if ($(this).val()) {
      $.ajax({
        type: 'POST',
         url: '/home/search',
        data: {
          val: $(this).val()
        },
        success: function(data) {
          var ul = $("<ul id='search_base' class='list-group'></ul>");
              ul.addClass('search_data');
              ul.css("left", "0");
              data.forEach((item) => {
          var li = $("<li></li>");
              li.html("<p><a href='/home/user/"+item['id']+"'>" + item['name'] + " " + item['surname'] + "</a></p>");
              li.data('user_id', item['id']);
              li.addClass("list-group-item d-flex justify-content-between align-items-center");
          if (item['request'] =='0') {
              li.append('<button class="btn btn-danger cancel_request">cancel request</button');
              } 
          else {
              li.append('<button class="btn btn-info send_request">send request</button');
              }
              li.children('p').prepend("<img src='" + item['photo'] + "' style='width: 30px;height: 30px;border-radius: 50%;margin-right: 5px;'>");
              ul.append(li);

          })
             $("#users_search").append(ul);
        }
      })
    }

  })



$(document).on("click",'.send_request',function(){
     var user_id = $(this).parent().data('user_id'),
         _this = this;
         $.ajax({
                type:'post',
                url:'/home/send/post',
                data:{user_id:user_id},
                success:function(data){
                
                    $(_this).removeClass('btn-info send_request').addClass('btn-danger cancel_request');
                    $(_this).html('cancel request');
                  
                }
          })
})
$(document).on("click",'.cancel_request',function(){
   var user_id = $(this).parent().data('user_id'),
        _this = this;
         $.ajax({
                type:'post',
                url:'/home/cancel/post',
                data:{user_id:user_id},
                success:function(data){

                         $(_this).removeClass('btn-danger cancel_request').addClass('btn-info send_request');
                         $(_this).html('send request');
                     
                }
          })
})

//stugum e ynkerutyan hardzum ka te voch
setTimeout(function run() {
  viewrequest();
  setTimeout(run, 10000);
}, 10000);
viewrequest();
function viewrequest(){
  $.ajax({
    type:'post',
    url:'/home/request/post',
    success:function(data){
      if (data.length!=0) {
            $(".plus_ sup").html(data.length).addClass("sup");
            $(".plus_ i").addClass("i");
       var ul = $("<ul id='viewrequest' class='list-group'></ul>");
            ul.addClass('search_data');
            ul.css({
              'display': "none",
              'right': '0'
            });
      data.forEach((item) => {
       var li = $("<li></li>");
            li.data('user_id', item['id']);
            li.addClass("list-group-item d-flex justify-content-between align-items-center");
            li.append("<a href='#'>"+
                          "<img src='"+item['photo']+"' style='width: 30px;height: 30px;border-radius: 50%;margin-right: 5px;'>"+
                          " "+item['name']+' '+item['surname']+" "+
                      "</a>")
            li.append('<button class="btn btn-info acceptRequest">accept</button');
            li.append('<button class="btn btn-danger cancelRequest">cancel</button');
            ul.append(li);
         })
            $("#user_plus").append(ul);
        } 
      else {
          $(".plus_ sup").html("").removeClass("sup");
          $(".plus_ i").removeClass("i")
        }
      }
          
        
    
  })
}

$(document).on('click','#user_plus',function(){
  $('#viewrequest').toggle();
})
$(document).on("click",'.acceptRequest',function(){
  var user_id = $(this).parent().data("user_id"),
      _this = this;
      $.ajax({
            type:'post',
            url:'/home/acceptrequest/post',
            data:{user_id:user_id},
            success:function(data){
            $(_this).parent().remove();
            MyFriends();
             viewrequest();
                 }
           })
})
$(document).on("click",'.cancelRequest',function(){
  var user_id = $(this).parent().data('user_id'),
      _this = this;
      $.ajax({
        type:'post',
        url:'/home/cancelreq/post',
        data:{
          user_id:user_id
        },
        success:function(data){
         $(_this).remove();
         viewrequest();
        }
      })
  
})
MyFriends();
function MyFriends(){
  $.ajax({
        type:'post',
        url:'/home/myfriends/post',
        success:function(data){
           $('aside').empty();
            data.forEach((item) => {
            var ul = $('<ul class="list-group friend"></ul>');
                ul.data("user", item);
            var li = $('<li class=" list-group-item list-group-flush list-group-item-action d-flex justify-content-between align-items-center"></li>');
            var svg = $('<svg width="16" height="16" >'+
                             '<circle  class="online-btn'+item['id']+'" cx="8" cy="8" r="5"  stroke-width="1" fill="transparent" />'+
                         '</svg>');
            var a = $('<a href="/home/user/'+item['id']+'" class="list-group-item"></a>');
                a.append("<img src='" + item['photo'] + "' width = '30'height = '30' style='border-radius:50%' >");
                a.append("<span> " + item['name'] + " " + item['surname'] + "</span>");
                a.prepend(svg);
                li.append(a);
                li.append('<span class="open_message badge badge-success badge-pill">'+
                            '<i class="fa fa-envelope-o"></i>'+
                            '<sup style="font-size:1.6em;color:red"></sup>'+
                          '</span>');
                ul.append(li);
            $("aside").append(ul);
          })
        
        }
  })
}


// usersonline
setInterval(()=>{
  OnlineUsers();
},1000);
  function OnlineUsers(){
    
    $.ajax({
      type:'post',
      url:'/home/usersonline',
      data:{},
      success:function(data){
        $('aside circle').attr('fill','red');
        $("#online-text i,#guest i").css('color','red').html('offline');
        data.forEach((item)=>{
          if(item['online'] =='1'){
              $('.online-text'+item['user_id']).css('color','blue').html('online');
              $('.online-btn'+item['user_id']).attr('fill','blue');
          }

        })
      }
    })
  }




});


//start socket ---------------------------------------------------------------------------------









//end socket ------------------------------------------------------------------------------------