$(document).ready(function() {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $("[data-toggle=tooltip]").tooltip();

  getNewPost();

  function getNewPost() {
    $.ajax({
      type: 'post',
      url: "/home/getpost",
      success: function(data) {
        if (data) {
          data.forEach((item) => {
            
            if(item['post_hide'] == 0){
            var div = $('<div class="user_post card gedf-card"></div');
            div.data("share_id", item['id']);
            var div1 = '<div class="card-header">' +
              '<div class="d-flex justify-content-between align-items-center">' +
              '<div class="d-flex justify-content-between align-items-center">' +
              '<div class="mr-2">' +
              '<img class="rounded-circle" width="45" src="' + item['avatar'] + '" alt="">' +
              '</div>' +
              '<div class="ml-2">' +
              '<div class="h5 m-0">' + '<a href ="home/user/' + item['my_id'] + '" style="font-size:15px;text-decoration:none;">' + item["name"] + ' ' + item["surname"] + '</a></div>' +
              '</div>' +
              '</div>' +
              '<div>' +
              '</div>';
            div.append(div1);
            var img = $("<img src='photo/menu-icon.png' class='post_icon'>")
            img.css({
              'position': 'absolute',
              "right": "0",
              "width": "20px",
              "height": "20px"
            })
            img.data("post_userid", item['user_id']);
            div.append(img);



            var div2 = $('<div class="card-body"></div');
            div2.append('<div class="text-muted h7 mb-2"> <i class="fa fa-clock-o"></i>' + item['updated_at'] + '</div>');
            if (item['text']) {
              div2.append('<h5 class="card-title text-primary">' + item['text'] + '</h5>');
            }
            if (item['photo']) {
              div2.append("<img width = '100%' height = '400px' src='" + item['photo'] + "'>");
            }
            if (item['video']) {
              div2.append('<video width="100%" height="300px" controls>' +
                '<source src="' + item['video'] + '">' +
                'Your browser does not support the video tag.' +
                '</video>');
            }
            if (item['audio']) {
              div2.append('<audio width="100%" controls>' +
                '<source src="' + item['audio'] + '">' +
                'Your browser does not support the video tag.' +
                '</audio>');
            }
            if (item['youtube']) {
              div2.append('<iframe width="100%" height="400" src="' + item['youtube'] + '"' +
                'frameborder="0" allow="autoplay; encrypted-media" allowfullscreen>' +
                '</iframe>');
            }
            div.append(div2);

            if (TestLikeShare(item['id']) == '1') {
              var button = '<button class="btn btn-outline-danger dislike_post"><span class="like-show like_count' + item["id"] + '">' + item["like_count"] + '</span> <i class="fa fa-gittip text-primary"></i> DisLike</button>';
            } else {
              var button = '<button class="btn btn-outline-primary like_post"><span class="like-show like_count' + item["id"] + '">' + item["like_count"] + '</span> <i class="fa fa-gittip text-primary"></i> Like</button>'
            }
            div.append('<div class="d-flex justify-content-around card-footer position-relative">' +
              button +
              "<button class='btn btn-outline-info comment_post'><span class='comment_count" + item['id'] + "'>" + item['comment_count'] + "</span> <i class='fa fa-comment'></i> Comment</button>" +
              '<button class="btn btn-outline-info"><i class="fa fa-mail-forward"></i> Share</button>' +
              '</div>')
            div.append("<div><input type='text' class='form-control commit p-3'></div>");
            div.append("<div  class='alert alert-dark share_comments w-100 p-1 comments" + item['id'] + "'></div>");

            $('#status').after(div);
          }

          })

           

        }
      }
    })
  }
  //post_menu button 
  $(document).on("mouseenter", '.post_icon', function() {

    var post_userid = $(this).data("post_userid"),
      post_id = $(this).parent().data("share_id"),
      user = $('input[name="user"]').data('user');
    if (user['id']) {
      var ul = $("<ul></ul>");
      ul.addClass("post-menu", 'list-group');
      ul.data("post_id", post_id);
      ul.data("user_id", user["id"]);
      if (user['id'] == post_userid) {
        ul.append(
          "<li class='delete_post list-group-item'>Delete Post</li>" +
          "<li class='close_post list-group-item'>Close Photo</li>"
        );
      } else {
        ul.append("<li class='close_post list-group-item'>Close Photo</li>");
      }
      $(this).parent().append(ul);
    }
    ul.fadeIn();
  })

  $(document).on("mouseleave", ".post-menu", function() {
    $(this).remove();
  })

  //delet post
  $(document).on("click", ".delete_post", function() {
    var share_id = $(this).parent().data('post_id'),
        _this = this;
    $.ajax({
         type: 'post',
         url:"/home/profile/timeline/delete",
         data: {
               post_id: share_id
               },
      success: function(r) {
        $(_this).parents(".user_post").remove();
      }

    })

  })
  //close post
  $(document).on("click", ".close_post", function() {

    var post_id = $(this).parent().data("post_id"),
        _this = this;
    $.ajax({
           type: 'post',
           url: '/home/profile/timeline/hide',
           data: {
              post_id: post_id
            },
      success: function(r) {
        $(_this).parents(".user_post").remove();
      }

    })
  })
  $(document).on("click", '.like_post', function() {
    var share_id = $(this).parents(".user_post").data('share_id');
    var _this = this;
    $.ajax({
      type: 'post',
      url: '/home/likeshare',
      data: {
        share_id: share_id
      },
      success: function(data) {
        $(".like_count" + share_id).html(data);
        $(".like_count" + share_id).parents("p").find("i").removeClass("text-white").addClass("text-primary");
        $(_this).removeClass("like_post btn-outline-primary").addClass("btn-outline-danger dislike_post").html(data + " DisLike");
      }
    })
  })
  $(document).on("click", ".dislike_post", function() {
    var share_id = $(this).parents(".user_post").data('share_id');
    var _this = this;
    $.ajax({
      type: 'post',
      url: '/home/dislikeshare',
      data: {
        share_id: share_id
      },
      success: function(data) {
        $(".like_count" + share_id).html(data);
        $(".like_count" + share_id).parents("p").find("i").removeClass("text-primary").addClass("text-white");
        $(_this).removeClass("btn-outline-danger dislike_post").addClass("btn-outline-primary like_post").html(data + " Like");
      }
    })
  })
  $(document).on("click", ".comment_post", function() {
    $(this).parents(".user_post").find(".commit").focus();
  })

  $(document).on("keydown", ".commit", function(e) {
    getAllShareComments();
    if (e.which == 13) {
      var text = $(this).val();
      var share_id = $(this).parents(".user_post").data("share_id");
      var _this = this;
      if (text) {
        $.ajax({
          type: 'post',
          url: '/home/commentshare',
          data: {
            share_id: share_id,
            comment: text
          },
          success: function(data) {
            $(".comment_count" + share_id).html(data['count']);
            $(_this).val("");
            InsertComment(data['id'], data['name'], data['surname'], data['photo'], text, data['time'], share_id);
          }
        })
      }
    }
  })
  //inser comment
  function InsertComment(id, name, surname, photo, text, time, share_id) {
    var control = "<div class='media  p-3' id='usre" + id + "'>" +
      '<img src="' + photo + '" alt="John Doe" class="mr-1 rounded-circle" style="width:40px;">' +
      "<div class='p-2 media-body alert-info' style='border-radius:10px;font-size:16px'>" +
      '<span>' +
      '<a href="/user/' + id + '" style="text-decoration:none;">' + name + " " + surname + ' </a>' +
      '<small><i>' + time + '</i></small>' +
      '</span><br>' +
      '<span>' + text + '</span>' +
      '</div>'
    '</div>';

    $(".comments" + share_id).prepend(control)
    // .scrollTop($(".comments"+share_id);
    // .prop('scrollHeight'));
  }


  var on;

  function TestLikeShare(post_id) {
    $.ajax({
      type: 'post',
      url: '/home/testlikeshare',
      data: {
        post_id: post_id
      },
      success: function(data) {
        on = data;
      },
      async: false
    })
    return on;
  }

  getAllShareComments();

  function getAllShareComments() {
    $.ajax({
      type: 'post',
      url: '/home/getallcomments',
      success: function(data) {
        if (data) {
          $(".share_comments").each(function() {
            $(this).empty();
          });
          data.forEach((item) => {
            InsertComment(item['id'], item['name'], item['surname'], item['photo'], item['comment'], item['updated_at'], item['shid']);
          })
        }
      }
    })
  }


});