$(document).ready(function() {

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$('.test-email').click(function() {
			var email = $('#email').val();
			$.ajax({
					type: 'post',
					url: '/login/forgotpasswordd/email',
					data: {
						email: email
					},
					success: function(data) {
						if (data != "1") {

							$('.info-message').css("color", 'red').html(data);
						} else {
							$('.info-message').css('color', 'black').html('enter the code from your email');
							$(".form-forgot").empty();
							$(".form-forgot").append('<div class="form-group">' +
								                            '<input type="text" id="test-code"  name="forgotnumber" class="form-control" placeholder="code">' +
								                     "</div>");
							$('.form-forgot').append('<input name="recover-submit" class="test-code btn btn-lg btn-primary btn-block" value="Forgot Password" type="button">');
						}
					}

				})
			})


$(document).on('click', '.test-code', function() {
	var code = +($('#test-code').val());
	$.ajax({
		type: 'post',
		url: '/login/forgotpasswordd/code',
		data: {
			code: code
		},
		success: function(data) {
			if (data=='1') {
				$('.info-message').css("color", 'black').html('Enter a new password');
				$(".form-forgot").empty();
				$(".form-forgot").append('<div class="form-group">' +
					'<input type="password" id="forgotpassword"  name="forgotpassword" class="form-control" placeholder="new pasword">' +
					'<input type="password" id="forgotconfirm"  name="password_confirmation" class="mt-2 mb-2 form-control" placeholder="confirm pasword">' +
					"</div>");
				$('.form-forgot').append('<input name="recover-submit" class="btn-forgotpassword btn btn-lg btn-primary btn-block" value="Forgot Password" type="button">');

			} 
			else {
				$('.info-message').css("color", 'red').html(data);
			}
		}
	})
}) 
$(document).on("click", ".btn-forgotpassword", function() {
	var password = $("#forgotpassword").val();
	var confirm = $("#forgotconfirm").val();
	$.ajax({
		type: 'post',
		url: '/login/forgotpasswordd/forgot',
		data: {
			password: password,
			password_confirmation: confirm
		},
		success: function(data) {
			if (data) {
				$('.info-message').css("color", 'red').html(data);
			} else {
				window.location.href = "http://127.0.0.1:8000/login";
			}
		}
	})
})

});