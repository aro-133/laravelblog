$(document).ready(function() {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

   $(document).on("click",'.friend-cancel-request',function(){
           var user_id = $(this).parents(".friends-friend-list").data("friend_id"),
               _this=this;
               $.ajax({
                type:'post',
                url:'friends/cancel',
                data:{user_id:user_id},
                success:function(data){

                  $(_this).parents(".friends-friend-btn").empty().append('<button class="btn btn-info pull-right friend-send-request">Add Friend</button>');

                }
      })
   })
    $(document).on("click",'.friend-accept-request',function(){
           var user_id = $(this).parents(".friends-friend-list").data("friend_id"),
               _this=this;
               $.ajax({
                type:'post',
                url:'friends/accept',
                data:{user_id:user_id},
                success:function(data){

                  $(_this).parents(".friends-friend-btn").empty().append('<button class="btn btn-danger pull-right unfriend">UnFriend</button>');

                }
      })
   })
     $(document).on("click",'.friend-unfriend',function(){
           var user_id = $(this).parents(".friends-friend-list").data("friend_id"),
               _this=this;
               $.ajax({
                type:'post',
                url:'friends/unfriend',
                data:{user_id:user_id},
                success:function(data){

                  $(_this).parents(".friends-friend-btn").empty().append('<button class="btn btn-info pull-right friend-send-request">Add Friend</button>');

                }
      })
   })
$(document).on("click",'.friend-send-request',function(){
           var user_id = $(this).parents(".friends-friend-list").data("friend_id"),
               _this=this;
               $.ajax({
                type:'post',
                url:'friends/sendrequest',
                data:{user_id:user_id},
                success:function(data){

                  $(_this).parents(".friends-friend-btn").empty().append('<button class="btn btn-warning pull-right friend-cancel-request">Cancel Request</button>');

                }
      })
   })


})