$(document).ready(function() {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $('#edit-about').click(function() {
    $('#edit-info').toggle(500);
    $("#edit-reset-password").hide();
    $('input[name="name"]').val($('.social-user-name').text());
    $('input[name="surname"]').val($('.social-user-surname').text());
    $('input[name="email"]').val($('.social-user-email').text());
    var gender = $('.social-user-gender').text();
    $('input[name="radio"]').each(function() {
      if ($(this).val() == gender) {
        $(this).attr("checked", 'checked');
        return;
      }

    })


  })
  $("#edit-contact").click(function() {
    $("#edit-contact-info").toggle(500);

  })
  $("#edit-work").click(function() {
    $("#edit-contact-work").toggle(500);
  })
  $("#edit-reset").click(function(){
    $("#edit-reset-password").toggle(500);
    $("#edit-info").hide();
  })



  // edit info edit-about
  $(document).on("click", '#edit-save', function() {
    var name = $("input[name='name']").val(),
      surname = $("input[name='surname']").val(),
      email = $("input[name='email']").val(),
      gender = $("input[name='radio']:checked").val(),
      date = $("input[name='birthday']").val();

    $.ajax({
      type: 'post',
      url: 'about/editinfo',
      data: {
        name: name,
        surname: surname,
        email: email,
        gender: gender,
        birthday: date
      },
      success: function(data) {
        $(".error-info").remove();
        if (data != "success") {

          for (var item in data) {
            $("#edit-info input").each(function() {
              if ($(this).attr('name') == item) {
                $(this).before('<p class="w-100 p-0 alert alert-danger error-info">' + data[item][0] + '</p>')
              }
            })
          }

        } else {
          swal({
               title: "Good job!",
               text: "click the update",
               icon: "success",
               button: true
              })
          .then((willDelete) => {
                 if (willDelete) {
                  window.location.reload(true);
                }               

         });
        }
      }
    })
  })

 $(document).on('click',"#edit-reset-save",function(){
    var oldpassword  = $(".reset-oldpassword").val(),
        password = $(".reset-password").val(),
        password_confirmation = $(".reset-password_confirmation").val();
    $.ajax({
      type:'post',
      url:'/home/chengepassword',
      data:{oldpassword:oldpassword,password:password,password_confirmation:password_confirmation},
      success:function(data)
      {
        $(".error-reset-password").remove();
        if(data == 'success'){
            swal({
               title: "Good job!",
               text: "click the update",
               icon: "success",
               button: true
              })
          .then((willDelete) => {
                 if (willDelete) {
                  window.location.reload(true);
                }               

         });
        }
        else
        {
          for (var item in data) {
            $("#edit-reset-password input").each(function() {
              if ($(this).attr('name') == item) {
                $(this).before('<p class="w-100 p-0 alert alert-danger error-reset-password">' + data[item][0] + '</p>')
              }
            })
          }
        }
      }
    })    

 })
 $(document).on("click","#edit-reset-cancel",function(){
     $("#edit-reset-password").hide(500);
 })


});