$(document).ready(function() {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });



$(document).on("click",'.like_post',function(){
  var share_id = $(this).parents(".user_post").data('share_id');
  var _this=this;
    $.ajax({
      type:'post',
      url:'/home/likeshare',
      data:{share_id:share_id},
      success:function(data){
              $(".like_count"+share_id).html(data);
              $(".like_count"+share_id).parents("p").find("i").removeClass("text-white").addClass("text-primary");
              $(_this).removeClass("like_post btn-outline-primary").addClass("btn-outline-danger dislike_post").html(data+ " DisLike");
      }
    })
})
$(document).on("click",".dislike_post",function(){
  var share_id = $(this).parents(".user_post").data('share_id');
  var _this=this;
    $.ajax({
      type:'post',
      url:'/home/dislikeshare',
      data:{share_id:share_id},
      success:function(data){
              $(".like_count"+share_id).html(data);
              $(".like_count"+share_id).parents("p").find("i").removeClass("text-primary").addClass("text-white");
              $(_this).removeClass("btn-outline-danger dislike_post").addClass("btn-outline-primary like_post").html(data+" Like");
      }
    })
})
$(document).on("click",".comment_post,.fa-comment",function(){
  $(this).parents(".user_post").find(".commit").focus();
})
$(document).on("keydown",".commit",function(e){
  getAllShareComments();
  if(e.which==13){
  var text = $(this).val();
  var share_id = $(this).parents(".user_post").data("share_id");
  var _this=this;
  if(text){
    $.ajax({
      type:'post',
      url:'/home/commentshare',
      data:{share_id:share_id,comment:text},
      success:function(data){
                      $(".comment_count"+share_id).html(data['count']);
                      $(_this).val("");
                      InsertComment(data['id'],data['name'],data['surname'],data['photo'],text,data['time'],share_id);
      }
    })
  }
}
})
//inser comment
function InsertComment(id,name,surname,photo,text,time,share_id){
      var control="<div class='media  p-3' id='usre"+id+"'>"+
                      '<img src="'+photo+'" alt="John Doe" class="mr-1 rounded-circle" style="width:40px;">'+
                          "<div class='p-2 media-body alert-info' style='border-radius:10px;font-size:16px'>"+
                             '<span>'+
                               '<a href="home/user/'+id+'" style="text-decoration:none;">'+name+" "+surname+' </a>'+
                               '<small><i>'+time+'</i></small>'+
                            '</span><br>'+
                              '<span>'+text+'</span>'+     
                           '</div>'
                      '</div>';          

     $(".comments"+share_id).prepend(control)
     // .scrollTop($(".comments"+share_id);
      // .prop('scrollHeight'));
}

getAllShareComments();
function getAllShareComments(){
  $.ajax({
    type:'post',
    url:'/home/getallcomments',
    success:function(data){
      if(data){
        $(".share_comments").each(function(){
          $(this).empty();
        });
        data.forEach((item) => {
          InsertComment(item['id'],item['name'],item['surname'],item['photo'],item['comment'],item['updated_at'],item['shid']);
        })
      }
    }
  })
}

// delete post
$(document).on('click',".post-delete",function(){
  var post_id = $(this).parents('.user_post').data('share_id'),
      _this=this;
      $.ajax({
        type:'post',
        url:'timeline/delete',
        data:{post_id:post_id},
        success:function(data){
          $(_this).parents(".user_post").remove();
        }
      })
});
$(document).on("click",".post-hide",function(){
  var post_id = $(this).parents('.user_post').data('share_id'),
      _this=this;
        console.log(post_id);
      $.ajax({
        type:'post',
        url:'timeline/hide',
        data:{post_id:post_id},
        success:function(data){
          $(_this).parents(".user_post").remove();
        }
      })
});
$(document).on("click",'.timeline .fa-thumbs-o-up',function(){
      $("#post-likeshow").remove();
      showlikes(this);
      $("#post-likeshow").fadeIn(1000);
})
function showlikes(_this){
  var post_id = $(_this).parents(".user_post").data("share_id");
      likecount = $(_this).parents(".user_post").find(".like_count"+post_id).html();
      $.ajax({
        type:'post',
        url:"timeline/showlikes",
        data:{post_id:post_id},
        success:function(data){
            if(data.length != 0){
            var div = $("<div id='post-likeshow' class='list-group'></div>");
                div.append("<div class='alert alert-dark list-group-item d-flex justify-content-between'>"+
                             "<i class='fa fa-thumbs-o-up text-primary'> "+likecount+"</i>"+
                             '<button class="btn btn-danger">X</button>'+
                          "</div>")
            data.forEach((item)=>{
                 div.append('<div class="list-group-item list-group-item-action">'+
                                "<img src="+item['photo']+">"+
                                '<a href="/home/user/'+item['id']+'" >'+item['name']+" "+item['surname']+'</a>'+
                             '</div>'
                             );
            })
            $(_this).before(div);
          }
        }
      })
}
$(document).on('click','#post-likeshow .btn',function(){
     $("#post-likeshow").remove();
})
$(document).on("mouseleave", "#post-likeshow", function() {
  $(this).remove();
})


});