$(document).ready(function() {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

$(document).on("mouseenter", ".box", function() {
  $(".photosline .box-cover").remove();
  var div = $("<div></div>");
      div.addClass("box-cover");
      div.append("<img class='menu-icon' src='/photo/menu-icon.png' width='30px' height='30px' style='float:right'>");
 $(this).append(div);
})

$(document).on("mouseleave", ".box", function() {
  $(".box-cover").remove();
})

$(document).on("mouseenter", ".menu-icon", function() {
  $(".photosline .photo-menu").remove();
  var ul = $("<ul></ul>");
      ul.addClass("photo-menu", 'list-group');
      ul.append(
               "<li class='make_profile list-group-item'>Make Profile Photo</li>" +
               "<li class='make_cover list-group-item'>Make Cover Photo</li>" +
               "<li class='delete_photo list-group-item'>Delete Photo</li>"
               );
  $(this).parents('.box').append(ul);
})

$(document).on("mouseleave", ".photo-menu", function() {
      $(".photosline .photo-menu").remove();
})


//change profile photo
$(document).on("click", ".make_profile", function(event) {
  event.stopPropagation();
  var picture_id = $(this).parents(".box").data("photo_id");
  console.log(picture_id);
  swal({
      title: "Are you sure?",
      text: "After installation, the profile photo will be changed",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {

        $.ajax({
          type: 'post',
          url: 'page/chengephoto',
          data: {
            picture_id: picture_id
          },
          success: function(data) {
            if (data) {
              $(".page_profile_photo").attr("src", data)
            }
          }
        })
        swal("profile photo changed", {
          icon: "success",
        });
      } else {
        swal("action rejected");
      }
    });
})

// change profiel cover

$(document).on("click", ".make_cover", function(event) {
  event.stopPropagation()
  var picture_id = $(this).parents(".box").data("photo_id");
  swal({
      title: "Are you sure?",
      text: "After installation, the cover photo will be changed",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
          type: 'post',
          url: 'page/chengecover',
          data: {
            picture_id: picture_id
          },
          success: function(data) {
            if (data) {
              $("#page_profile_cover").attr("src", data);
            }

          }
        })
        swal("cover photo changed", {
          icon: "success",
        });
      } else {
        swal("action rejected");
      }
    });
})

// delete profile photo
$(document).on("click", ".delete_photo", function(event) {
  event.stopPropagation();
  var picture_id = $(this).parents(".box").data("photo_id"),
     _this=$(this).parents(".box");
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this imaginary file!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {

        _this.remove();
        $.ajax({
          type: 'post',
          url: 'page/deletephoto',
          data: {
            picture_id: picture_id
          },
          success: function(data) {
          
            if(data['cover'])
            {
              $("#page_profile_cover").attr("src", data['cover']);
            }
            else
            {
              $(".page_profile_photo").attr("src", data['profile']);
            }
          }
        })
        swal("Poof! Your imaginary file has been deleted!", {
          icon: "success",
        });
            
      } else {
        swal("Your imaginary file is safe!");
      }
    });
})






$(document).on("click", ".box", function() {
  var item = $(this).children("img").attr("id");
  $(".open-slides-show").remove();
  var div = $("<div class='open-slide-show row'></div>");
      div.append("<div class='col-12 col-sm-8 image_bar'>"+
                    "<button class='close-slide-show btn btn-danger'>X</button>"+
                    '<button class="w3-button w3-black w3-display-left" id="plusDivs">&#10094;</button>'+
                    '<button class="w3-button w3-black w3-display-right" id="minusDivs">&#10095;</button>'+
                    "<div class='slidefooterbar bg-dark w-100 p-0'>"+
                         "<div class='likes-photo-bar position-relative'><span style='font-size:20px' class='text-white page-photo-countlike'></span> <i class='fa fa-heart text-white page-photo-like'> Like</i></div>"+
                         "<div><span style='font-size:20px' class='text-white page-photo-countcomment'></span> <i class='fa fa-comment text-white page-photo-comment'> Comment</i></div>"+
                    "</div>"+
                 "</div>");
  div.append("<div class='col-12 col-sm-4 comment_bar  p-0'>"+
                 '<div class="photo-comments-bar alert alert-success"></div>'+
                 "<input class='form-control' id='comments_input'>"+
             "</div> ");
  $(".photosline").before(div);
  slideIndex = item;
  // $("body").animate({
  //         scrollTop:1500
  //     },2000)
  showDivs(slideIndex);
  div.fadeIn(1000);
})
$(document).on("click","#plusDivs",function(){
  plusDivs(-1);
})
$(document).on("click","#minusDivs",function(){
  plusDivs(1);
})


var slideIndex;

function plusDivs(item) {
  showDivs(slideIndex += item);
}

function showDivs(item) {

  var array_img = document.getElementsByClassName("mySlides");
  if (item > array_img.length) 
  {
    slideIndex = 1
  }
  if (item < 1) 
  {
    slideIndex = array_img.length
  }


  $(".open-slide-show .mySlides").each(function() {
    $(this).remove();
  })

  var photo_id = $(array_img[slideIndex]).data('picture_id');
      GetAllPhoto(photo_id);
  $(array_img[slideIndex]).clone().appendTo(".image_bar");
}

$(document).on("click", ".close-slide-show", function() {
  $(".open-slide-show").remove();
})
// end

$(document).on("click",".page-photo-like",function(){
   var photo_id = $(".image_bar .mySlides").data("picture_id"),
       _this = this;
      GetAllPhoto(photo_id);
       $.ajax({
         type:'post',
         url:'/home/page/likephoto',
         data:{photo_id:photo_id},
         success:function(data){
           $('.page-photo-countlike').html(data);
           $(_this).removeClass("text-white page-photo-like").addClass("text-primary page-photo-dislike");
         }
       })
})
$(document).on("click",".page-photo-dislike",function(){
   var photo_id = $(".image_bar .mySlides").data("picture_id"),
       _this = this;
       GetAllPhoto(photo_id);
       $.ajax({
         type:'post',
         url:'/home/page/dislikephoto',
         data:{photo_id:photo_id},
         success:function(data){
          console.log(data);
           $('.page-photo-countlike').html(data);
           $(_this).removeClass("text-primary page-photo-dislike").addClass("text-white page-photo-like");
           
         }
       })
})
$(document).on("click",".page-photo-comment",function(){
    $("#comments_input").focus();
})

 $(document).on("change","#comments_input",function(){
   var  text = $(this).val(),
        photo_id = $(this).parents(".open-slide-show").find('.mySlides').data("picture_id"),
       _this = this;
   if(text)
   {    
        $(this).val("");
        $.ajax({
          type:'post',
          url:'/home/page/commentphoto',
          data:{photo_id:photo_id,text:text},
          success:function(data){
            console.log(data)          
              var count = +($(".page-photo-countcomment").html());
                $(".page-photo-countcomment").html(count+1);
            InsertComment(data['id'],data['name'],data['surname'],data['photo'],text,data['date']);
          }
        })

   }
 })
 function GetAllPhoto(photo_id)
 {
   $.ajax({
     type:'post',
     url:'/home/page/getallphoto',
     data:{photo_id:photo_id},
     success:function(data){
       console.log(data[0],photo_id);
       var likes = data[0];
       var comments = data[1];
           $('.image_bar').find('i').removeClass("text-primary page-photo-dislike").addClass("text-white page-photo-like");
           $(".page-photo-countlike").html(likes.length);
           $(".page-photo-countcomment").html(comments.length);
           $(".photo-comments-bar").empty();
      comments.forEach((item)=>{
             InsertComment(item['id'],item['name'],item['surname'],item['photo'],item['comment'],item['date']); 
       })


      
        likes.forEach((item)=>{
            if(item['count'] != 0 ){
               $('.image_bar').find('i').removeClass("text-white page-photo-like").addClass("text-primary page-photo-dislike");
            }
            else
            {
              $('.image_bar').find('i').removeClass("text-primary page-photo-dislike").addClass("text-white page-photo-like");
            }
         })


        $(".likes-photo-bar #photo-likeshow").remove();
        var box = $("<div id='photo-likeshow' class='list-group'></div>");
                box.append("<div class='alert alert-dark list-group-item d-flex justify-content-between'>"+
                             "<i class='fa fa-heart text-danger'> "+likes.length+"</i>"+
                             '<button class="btn btn-danger">X</button>'+
                          "</div>")
            likes.forEach((item)=>{
              console.log(item);
                 box.append('<div class="list-group-item list-group-item-action">'+
                                "<img src="+item['photo']+">"+
                                '<a href="/home/user/'+item['id']+'" >'+item['name']+" "+item['surname']+'</a>'+
                             '</div>'
                             );
            })
            $(".likes-photo-bar").append(box);
     }
   })
 }
$(document).on("click",".page-photo-countlike",function(){
   $("#photo-likeshow").toggle(500);
})
$(document).on('click','#photo-likeshow .btn',function(){
     $("#photo-likeshow").hide(500);
})





 function InsertComment(id, name, surname, photo, text, time) {
    var control = "<div class='media  p-1' id='usre" + id + "'>" +
      '<img src="' + photo + '" alt="" class="mr-1 rounded-circle" style="width:20px;">' +
      "<div class='p-1 media-body alert-info' style='border-radius:10px;font-size:15px'>" +
      '<span>' +
      '<a style="font-size:15px;" href="/home/user/' + id + '" style="text-decoration:none;">' + name + " " + surname + ' </a>' +
      '<small><i>' + time + '</i></small>' +
      '</span><br>' +
      '<span>' + text + '</span>' +
      '</div>'
    '</div>';

    $(".photo-comments-bar").append(control).scrollTop($(".photo-comments-bar").prop('scrollHeight'));
}

});