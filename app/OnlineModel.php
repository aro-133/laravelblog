<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnlineModel extends Model
{
    //
    protected $table = 'usersonline';
    public $timestamps = false;
    
}
