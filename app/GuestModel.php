<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuestModel extends Model
{
    //
    protected $table="guest";
    public $timestamps = false;
}
