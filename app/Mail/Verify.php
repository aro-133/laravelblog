<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Hash;
use App\User;
class Verify extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public $id;
    public function __construct(User $user)
    {
      $this -> user = $user;
      $id = base64_encode($this -> user -> id);
      $url = "http://127.0.0.1:8000/verify/".$id;
      $this -> url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('verify')
                    ->with("url",$this -> url);
    }
}
