<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikePhotoModel extends Model
{
    //
    protected $table = 'likephoto';
    public $timestamps = false;
}
