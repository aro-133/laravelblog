<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatsModel extends Model
{
    //
    protected $table ='chats';
    public $timestamps =false;
    protected $guarded = ['created_at','updated_at'];

}
