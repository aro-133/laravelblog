<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersPhotoModel extends Model
{
    //
    protected $table = 'usersphotos';
}
