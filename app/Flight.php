<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Flight extends Model
{
    //  
        use SoftDeletes;
        protected $dates = ['deleted_at'];

        protected $table = 'flight';
        // knayi lrutyamb trvac blog tb i miji flight tablen
        public $timestamps = false;   
        // vor cogtagirci banali barern  
        protected $dateFormat = 'U';
        // kadirovkan e talis 
        // protected $connection = 'connection-name' talis e bazayi anun ete chenq uzum vercnilrutyamb trvacic
        
       Schema::table('flight', function ($table) {
                $table->softDeletes();
         });
}
