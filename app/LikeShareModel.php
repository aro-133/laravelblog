<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeShareModel extends Model
{
    //
    protected $table = 'postlikes';
    public $timestamps = false;
}
