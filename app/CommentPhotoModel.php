<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentPhotoModel extends Model
{
    //
    protected $table = 'commentphoto';
    public $timestamps = false;
}
