<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\RegisterController;
use App\User;
use App\ShareModel;
use App\Friends;
use App\LikeShareModel;
use App\CommentShareModel;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\PostHideModel;
class ShareController extends Controller
{
    //
    public function share(Request $request){
       $share = new ShareModel;
       $user = Auth::user();
       $share -> user_id = $user['id'];
       $bool = false;
       $text = $request -> input('share_text');
       if(isset($text)){
       	$bool = true;
       	     if(strstr($text,"www.youtube.com")){
       	     	$txt = strstr($text,"https:",true);
				$text = strstr($text,"https:");
				$youtube = str_replace("watch?v=","embed/",$text);
				$youtube = str_replace("&","?",$youtube);
				$share -> text = $txt;
				$share -> youtube = $youtube;
       	     }
       	     else{
       	     	$txt = $text;
       	     	$share -> text = $txt;
       	     }
       }
        $photo = $request -> file("share_photo");
        if(isset($photo)){
        	$bool = true;
                $photo->move(public_path().'/share/photo', date('dmY_Hi').'_'.$photo->getClientOriginalName());
                $photo ='/share/photo/'.date('dmY_Hi').'_'.$photo->getClientOriginalName();
                $share ->  photo = $photo;
         }
        $video = $request -> file("share_video");
        if(isset($video)){
        	$bool = true;
                $video->move(public_path().'/share/video', date('dmY_Hi').'_'.$video->getClientOriginalName());
                $video ='/share/video/'.date('dmY_Hi').'_'.$video->getClientOriginalName();
                $share -> video = $video;
         }
        $audio = $request -> file("share_audio");
        if(isset($audio)){
        	$bool = true;
                $audio->move(public_path().'/share/audio', date('dmY_Hi').'_'.$audio->getClientOriginalName());
                $audio = '/share/audio/'.date('dmY_Hi').'_'.$audio->getClientOriginalName();
                $share -> audio = $audio;
         }
         if($bool)
         {
             $time = date("Y-m-d H:i:s");
             $share -> time = $time;
             $share -> save();
            return redirect('/home');
         }
         else{
         	return redirect("/home")->with("post_share","post is empty");
         }
      }
      



      public function getpost(){
         $user = Auth::user();
         $friend1 = Friends::where('user2_id',$user['id'])->pluck("user1_id");
         $friend2 = Friends::where("user1_id",$user['id'])->pluck('user2_id');
         $users = $friend1->union($friend2);
         $users->prepend($user['id']);      
         $data = ShareModel::join('users','share.user_id','=','users.id')
                    ->select("share.*","users.name","users.surname","users.photo as avatar")
                    ->whereIn('share.user_id',$users)
                    ->get();
         
         foreach ($data as $key) {
         	$post_id = $key['id'];
         	$count = LikeShareModel::where('post_id',$post_id)
         	                        ->groupBy('post_id')
         	                        ->count('*');
         	$key['like_count'] = $count;
         	$count = CommentShareModel::where("post_id",$post_id)
         	                           ->groupBy("post_id")
         	                           ->count("*");
         	$key['comment_count'] = $count;
          $count = PostHideModel::where([
                                ["user_id",Auth::id()],
                                ["post_id",$post_id]
                              ])->count("*");
          $key['post_hide'] = $count;


         }
         return response()->json($data);
      	
      }
      public function likeshare(Request $request){
            $post_id = $request-> input('share_id');
            $user = Auth::user();
            LikeShareModel::insert(['user_id'=>$user['id'],'post_id'=>$post_id]);
            $count=LikeShareModel::where('post_id',$post_id)
                                   ->groupBy('post_id')
                                   ->count("*");
            return $count;
      }
      public function dislikeshare(Request $request){
            $post_id = $request-> input('share_id');
            $user = Auth::user();
            LikeShareModel::where([
            	                   ['user_id',$user['id']],
            	                   ['post_id',$post_id]
            	               ])->delete();
            $count=LikeShareModel::where('post_id',$post_id)
                                   ->count("post_id");
            return $count;
      }
      public function testlikeshare(Request $request){
            $post_id = $request -> input('post_id');
            $user = Auth::user();
            $count = LikeShareModel::where([
            	                  ['user_id',$user['id']],
            	                  ['post_id',$post_id]
            	              ])->count("*");
            return $count;
      }
      public function commentspost(Request $request){
      	    $post_id = $request -> input("share_id");
      	    $comment = $request -> input("comment");
      	    $user = Auth::user();
      	    $time = date('Y-m-d H:i:s');
      	    $commit = new CommentShareModel;
      	    $commit	-> user_id = $user['id'];
      	    $commit -> post_id = $post_id;
      	    $commit -> comment = $comment;
      	    $commit -> time = $time;
      	    $commit -> save();
      	    $count = CommentShareModel::where("post_id",$post_id)->count("post_id");
      	    $user['count'] = $count;
      	    $user['time'] = $time;
      	    return response()-> json($user);
      }
      public function getallcomments(){
      	    $data = CommentShareModel::join('users',"commentspost.user_id",'=','users.id')
      	                             ->join('share','commentspost.post_id','=','share.id')
      	                             ->select('users.*','share.id as shid','commentspost.comment','commentspost.created_at',"commentspost.updated_at")
      	                             ->get();
      	    return response()->json($data);
      }

    
}
