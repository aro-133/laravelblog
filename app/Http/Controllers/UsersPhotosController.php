<?php

namespace App\Http\Controllers;

use App\UsersPhotoModel;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\LikePhotoModel;
use App\CommentPhotoModel;
class UsersPhotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, UsersPhotoModel $usersPhotoModel)
    {
        $user = Auth::user();
        if($request->hasFile('photos')){
            foreach ($request->file('photos') as $photo) 
            {   
                $name = $photo->getClientOriginalName();
                $photo -> move(public_path().'/photo/'.$user['email'].'/',date('dmY_Hi').'_'.$name);
                $photo = '/photo/'.$user['email'].'/'.date('dmY_Hi').'_'.$name;
                $usersPhotoModel -> insert(['user_id'=>$user['id'],'photo'=>$photo]);
            }
                return redirect()->route('showphoto',['photo'=>"photo"]);
        }
        else{
        
            return back()->with('error',"in valid photos");
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UsersPhotoModel  $usersPhotoModel
     * @return \Illuminate\Http\Response
     */
    public function show(UsersPhotoModel $usersPhotoModel,$photo)
    {
        //
        $user = Auth::user();
        $gallery = $usersPhotoModel->where("user_id",$user['id'])->get();
        return view('profile',compact('user','photo','gallery'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UsersPhotoModel  $usersPhotoModel
     * @return \Illuminate\Http\Response
     */
    public function edit(UsersPhotoModel $usersPhotoModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UsersPhotoModel  $usersPhotoModel
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request, UsersPhotoModel $usersPhotoModel)
    {
        $data = $usersPhotoModel->where("id",$request->picture_id)
                                     ->pluck('photo');
        User::where('id',Auth::id())->update(['photo'=>$data[0]]);
        return $data[0];

    }
    public function updateCover(Request $request,UsersPhotoModel $usersPhotoModel)
    {
        $data = $usersPhotoModel->where("id",$request->picture_id)
                                       ->pluck('photo');
        User::where("id",Auth::id())->update(['cover'=>$data[0]]);
        return $data[0];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UsersPhotoModel  $usersPhotoModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsersPhotoModel $usersPhotoModel,Request $request)
    {
        //
        $user = Auth::user();
        $photo = $usersPhotoModel->where("id",$request-> picture_id)->pluck('photo');
        if($photo[0] == $user['photo'])
        {
            User::where('id',Auth::id())->update(['photo'=>'/photo/dphotoprofile.png']);
            $photo['profile'] = '/photo/dphotoprofile.png';
        }
        if($photo[0] == $user['cover'])
        {  
           User::where('id',Auth::id())->update(['cover'=>'/photo/cover.jpg']);
           $photo['cover'] = '/photo/cover.jpg';
        }
        unlink(public_path($photo[0]));
        $usersPhotoModel -> destroy($request -> picture_id);
        return response()-> json($photo);
    }

    public function LikePhoto(Request $request,LikePhotoModel $likephoto)
    {
        $likephoto -> insert(["user_id" => Auth::id(),"photo_id"=>$request -> photo_id]);
        $count = $likephoto->where("photo_id",$request->photo_id)->count("*");
        return $count;
    }
    public function DislikePhoto(Request $request,LikePhotoModel $likephoto)
    {
        LikePhotoModel::where([["user_id",Auth::id()],["photo_id",$request->photo_id]])->delete();
        $count = $likephoto->where("photo_id",$request->photo_id)->count("*");
        return $count;
    }
    public function CommentPhoto(Request $request,CommentPhotoModel $comment)
    {    
        $comment -> insert([
                   "user_id" => Auth::id(),
                   "photo_id" => $request -> photo_id,
                   "comment" => $request -> text,
                   'date' => date("Y-m-d H:i:s")
               ]);
        $user = Auth::user();
        $user['date'] = date("Y-m-d H:i:s");
        return response()->json($user);
    }
    public function GetAllPhoto(Request $request,CommentPhotoModel $comment,LikePhotoModel $like,UsersPhotoModel $photo)
    {
        $likes = $like->join("users","users.id","=","likephoto.user_id")
                      ->select("users.*")
                      ->where("likephoto.photo_id",$request -> photo_id)
                      ->get();
        $comments = $comment -> join("users","users.id","=","commentphoto.user_id")   
                             -> select("users.*","commentphoto.date","commentphoto.comment")  
                             -> where("commentphoto.photo_id",$request -> photo_id)
                             -> get();
        foreach ($likes as $key) {
            if($key['id']==Auth::id())
            {
                $count = $like ->where([
                      ['user_id',$key['id']],
                      ['photo_id',$request-> photo_id]
                  ])->count('*');

                $key['count'] = $count;

            }
            else
            {
                $key['count'] = '0';
            }
        }
        $data[]=$likes;
        $data[]=$comments;
        return response()->json($data);
    }
}
