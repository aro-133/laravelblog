<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\RequestModel;
use App\Friends;
use App\User;
use App\FollowsPageModel;
class FriendController extends Controller
{
    //
    protected $friend;
    public function __construct(){
       
    }
    public function show($id){
       $friend = User::where("id",$id)->first();
       $user = Auth::user();
       return view('friend',compact('friend','user'));
    }
    public function followsUserPage(Request $request){
    	$id = Auth::id();
    	$other_user = $request -> input('user_id');
    	FollowsPageModel::insert(['user1_id'=>$id,"user2_id"=>$other_user]);
        $user = FollowsPageModel::join('users','users.id','=','follows.user1_id')
                                ->where('user2_id',$other_user)
                                ->select("users.*")
                                ->get();
                                
    	return response()->json($user);

    }
    public function UnFollowsPage(Request $request){
    	$id = Auth::id();
    	$other_user = $request -> input('user_id');
    	FollowsPageModel::where([
    		             ['user1_id',$id],
    		             ['user2_id',$other_user]
    		         ])->delete();
        $user = FollowsPageModel::join('users','users.id','=','follows.user1_id')
                                ->where('user2_id',$other_user)
                                ->select("users.*")
                                ->get();
    	return response()->json($user);
    }
}
