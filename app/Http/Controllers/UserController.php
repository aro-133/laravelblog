<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
use App\Http\Controllers\Auth\RegisterController;
use App\RequestModel;
use App\Friends;
use App\User;
use App\ShareModel;
use App\LikeShareModel;
use App\CommentsShareModel;
use App\FollowsPageModel;
use App\UsersPhotoModel;
use App\GuestModel;
use App\Http\Controllers\PageController;

class UserController extends Controller
{
    public function __construct(){
    }

    public function store(Request $request){
    }

    public function home(){
    	$user = Auth::user();
      $email = strstr($user['email'],"@",true);
      session(['email'=>$email]);
    	return view('home',compact('user'));
    }

    public function searchusers(Request $request){
          
           if($request -> isMethod('post')){
               	$search = $request ->Input('val');
                $user = Auth::user();
                $user1 = Friends::Where('user2_id','=',Auth::id())->pluck('user1_id');
                $user2 = Friends::Where('user1_id','=',Auth::id())->pluck('user2_id');
                $data = User::where(function($query) use ($search) {
                             $query->Where('name','like',$search."%")
                                   ->orWhere('surname','like',$search."%");
                                   })
                            ->WhereNotIn('id',$user2->concat($user1)->concat([$user['id']]))
                            ->get();
            foreach ($data as $key) {
            	  $user2_id = $key ['id'];
            	  $query = RequestModel::where([
            		                      ['user1_id',$user['id']],
            	                       	['user2_id',$user2_id]
                                            ])
                                      ->get();
                if($query->isEmpty()){
                   $key['request']=1;
            	     }
            	 else{
            	   $key['request']=0;
            	    }
                 }
              return response()->json($data);
 
           }
           else{
                echo "get";
           }
       }

       public function myfriends(){
           $user = Auth::user();
           $user1_id = Friends::where('user2_id',$user['id'])
                                ->pluck('user1_id');
           $user2_id = Friends::where('user1_id',$user['id'])
                                ->pluck("user2_id");
                                
           $data = User::find($user1_id->concat($user2_id));
           return response()->json($data);
       }

    public function userProfile($id){
           $user = Auth::user();
           if($id == $user['id'])
           {
            $email = strstr($user['email'],"@",true);
            return redirect()->route('profile', ['eamil' => $email]);
           }
            $query = GuestModel::where([['user1_id',$user['id']],['user2_id',$id]])->pluck('id');

           if($query->IsNotEmpty())
           {  
               GuestModel::where('id',$query[0])->update(["date"=>date("Y-m-d H:i:s"),'status'=>0]);
           }
           else
           {
             GuestModel::insert(['user1_id'=>$user['id'],'user2_id'=>$id,'date'=>date("Y-m-d H:i:s")]);
           }
              

           $friend = User::where("id",$id)->first();
           $timeline = PageController::getTimeline($friend['id']);
           $friends = PageController::getFriends($friend['id']);
           $follow = FollowsPageModel::where('user2_id',$id)->pluck('user1_id');
           $follows = User::find($follow);
           $follow  = collect($follow)->search($user['id']);
           $gallery = UsersPhotoModel::where('user_id',$id)->get();

           return view('friend',compact('friend','user','timeline','friends','follows','follow','gallery'));
          
    }

       public function myProfile($email){

           $user = Auth::user();
           $gallery = UsersPhotoModel::where('user_id',$user['id'])->get();
           $timeline = PageController::getTimeline($user['id']);
           $friends = PageController::getFriends($user['id']);
           $follow = FollowsPageModel::where('user2_id',$user['id'])->pluck('user1_id');
           $follows = User::find($follow);
           $guest = GuestModel::where([
                      ["user2_id",$user['id']],
                      ['status',0]])->count("*");
           return view("profile",compact("user",'gallery',"timeline",'friends','follows','guest'));
       }

      

}