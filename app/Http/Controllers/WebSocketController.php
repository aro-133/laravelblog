<?php
namespace App\Http\Controllers;
// askedio/laravel-ratchet
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
//use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App;
use Auth;
use Config;
use Crypt;
use App\User;
use Illuminate\Session\SessionManager;

class WebSocketController extends Controller implements MessageComponentInterface{
    private $connections = [];
    
     /**
     * When a new connection is opened it will be passed to this method
     * @param  ConnectionInterface $conn The socket/connection that just connected to your application
     * @throws \Exception
     */
    function onOpen(ConnectionInterface $conn){
        echo "\non open";
        var_dump($conn);
        // $this->clients->attach($conn);
        // Create a new session handler for this client
        $session = (new SessionManager(App::getInstance()))->driver();
        // Get the cookies
        $cookies = $conn->WebSocket->request->getCookies();
        // Get the laravel's one
        $laravelCookie = urldecode($cookies[Config::get('session.cookie')]);
        // get the user session id from it
        $idSession = Crypt::decrypt($laravelCookie);
        // Set the session id to the session handler
        $session->setId($idSession);
        // Bind the session handler to the client connection
        $conn->session = $session;
    }
    
     /**
     * This is called before or after a socket is closed (depends on how it's closed).  SendMessage to $conn will not result in an error if it has already been closed.
     * @param  ConnectionInterface $conn The socket/connection that is closing/closed
     * @throws \Exception
     */
    function onClose(ConnectionInterface $conn){
        echo "\non close";
        
        $disconnectedId = $conn->resourceId;
        unset($this->connections[$disconnectedId]);
        foreach($this->connections as &$connection)
            $connection['conn']->send(json_encode([
                'offline_user' => $disconnectedId,
                'from_user_id' => 'server control',
                'from_resource_id' => null
            ]));
    }
    
     /**
     * If there is an error with one of the sockets, or somewhere in the application where an Exception is thrown,
     * the Exception is sent back down the stack, handled by the Server and bubbled back up the application through this method
     * @param  ConnectionInterface $conn
     * @param  \Exception $e
     * @throws \Exception
     */
    function onError(ConnectionInterface $conn, \Exception $e){
        echo "\non error";
        echo $e->getMessage();
        
        //$userId = $this->connections[$conn->resourceId]['user_id'];
        //var_dump($e);
        //echo "An error has occurred with user $userId: {$e->getMessage()}\n";
        //unset($this->connections[$conn->resourceId]);
        $conn->close();
    }
    
     /**
     * Triggered when a client sends data through the socket
     * @param  \Ratchet\ConnectionInterface $conn The socket/connection that sent the message to your application
     * @param  string $msg The message received
     * @throws \Exception
     */
    function onMessage(ConnectionInterface $conn, $msg) {
        // $conn->send(json_encode([
        //     'msg' => 'text'
        // ]));.
        //echo $msg->user_id ;//(string)$msg;
        //echo 'msg: ' . json_encode($this->connections[$conn->resourceId]);

        echo "\non message";
        
        // start the session when the user send a message 
        // (refreshing it to be sure that we have access to the current state of the session)
        // $from->session->start();
        // do what you wants with the session 
        // for example you can test if the user is auth and get his id back like this:
        // $idUser = $from->session->get(Auth::getName());
        // if (!isset($idUser)) {
        //     echo "the user is not logged via an http session";
        // } else {
        //     // $currentUser = User::find($idUser);
        //     var_dump(json_decode($currentUser));
        // }
        // or you can save data to the session
        //$from->session->put('foo', 'bar');
        // ...
        // and at the end. save the session state to the store
        // $from->session->save();
        //var_dump(json_decode($msg));

        //DB::insert('insert into message (value) values (?)', [$msg]);

        $conn->send('response eeee');

        // if (is_null($this->connections[$conn->resourceId]['user_id'])) 
        // {
        //     $this->connections[$conn->resourceId]['user_id'] = $msg;
        //     $onlineUsers = [];
        //     foreach($this->connections as $resourceId => &$connection)
        //     {
        //         $connection['conn']->send(json_encode([$conn->resourceId => $msg]));
        //         if($conn->resourceId != $resourceId)
        //             $onlineUsers[$resourceId] = $connection['user_id'];
        //     }
        //     $conn->send(json_encode(["gfhghghgh"]));
        // } 
        // else
        //  {
        //     $fromUserId = $this->connections[$conn->resourceId]['user_id'];
        //     $msg = json_decode($msg, true);
        //     $this->connections[$msg['to']]['conn']->send(json_encode([
        //         'msg' => $msg['content'],
        //         'from_user_id' => $fromUserId,
        //         'from_resource_id' => $conn->resourceId
        //     ]));
        // }
    }
 

}
