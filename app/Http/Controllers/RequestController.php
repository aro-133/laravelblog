<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\RequestModel;
use App\Friends;
use App\User;
class RequestController extends Controller
{
    
    protected $requests;
    public function __construct(){
          $this -> requests = new RequestModel;
    }
    public function sendrequest(Request $request){
    	  $user  = Auth::user();
    	  $user_id = $request -> Input('user_id');
    	  $this -> requests -> user1_id = $user['id'];
    	  $this -> requests -> user2_id = $user_id;
    	  $this -> requests -> save();

    }
    public function cancelrequest(Request $request){
    	$user_id = $request -> input('user_id');
        $user = Auth::user();
        RequestModel::where([
        	         ['user1_id',$user['id']],
        	         ['user2_id',$user_id]
        	               ])
                      ->delete();
    }
    public function viewrequest(){
    	$user = Auth::user();
    	$query = RequestModel::where("user2_id",$user['id'])->pluck('user1_id');
    	$data = User::find($query);
    	return response()->json($data);
    }
    public function acceptrequest(Request $request){
        $user = Auth::user();
        $user_id = $request -> Input('user_id');
        $friends  = new Friends;
        $friends -> user1_id = $user['id'];
        $friends -> user2_id = $user_id;
        $friends -> save();
        RequestModel::where([
        	          ['user1_id',$user_id],
        	          ['user2_id',$user['id']]
        	                ])
                      ->delete();
        return response()->json('ok');

    }
    public function cancelreq(Request $request){
        $user = Auth::user();
        $user_id = $request -> Input("user_id");
        RequestModel::where([
        	          ['user1_id',$user_id],
        	          ['user2_id',$user['id']]
        	                ])
                      ->delete();
        return response()->json('ok');
    }
}
