<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ChatsModel;
use App\User;
class ChatController extends Controller
{
    //
    public function SetMessage(Request $request)
    {
    	$message = e($request -> message);
    	$user_id = $request -> user_id;
    	ChatsModel::insert([
    		    'user1_id' => Auth::id(),
    		    'user2_id' => $user_id,
    		    'message' => $message,
    		    'time' => date("Y-m-d H:i:s")
    	    ]);

    	$user = Auth::user();
    	$user['message'] = $message;
    	$user["time"] = date("Y-m-d H:i:s");
    	return response()->Json($user);
    }
    public function GetMessage(Request $request)
    {
    	$user_id = $request -> user_id;
    	$id = Auth::id();
    	ChatsModel::where([
    		         ['user1_id',$request -> user_id],
    		         ['user2_id',$id]
    	                 ])
    	            ->orwhere(function($query)use($id,$user_id){
    	            	 $query->where([
                             	['user1_id',$id],
                             	['user2_id',$user_id],
                             ]);
    	            })->update(['status'=>1]);
    	$data = ChatsModel::where([
    		             ['user1_id',$id],
    		             ['user2_id',$user_id],
    		             ['status',1]
    		                      ])
    	                ->orwhere(function($query)use($user_id,$id){
                             $query->where([
                             	['user1_id',$user_id],
                             	['user2_id',$id],
                             	['status',1]
                             ]);
    	                  })->get();

    	foreach ($data as $key) {
    		$key['photo'] = User::where("id",$key['user1_id'])->pluck("photo")[0];
    	}
    	return response()->json($data);
    }
    public function SearchMessage()
    {


       $data = ChatsModel::where([
                         ["user2_id",Auth::id()],
                         ['status',0]
                     ])                        
                         ->groupBy('user1_id')
                         ->selectRaw("user1_id,count(user1_id) as count")
                         ->get();

       return response()->json($data);                  
    }
}
