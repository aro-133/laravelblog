<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\OnlineModel;
use Illuminate\Http\Request;
use Carbon\Carbon;
class OnlineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function UsersOnline(OnlineModel $onlineModel)
    {
        $data = $onlineModel ->all();
        foreach ($data as $key ) {
            if($key['lastactive'] < Carbon::now()->subMinutes(2)->format('Y-m-d H:i:s'))
            {
                $key['online']="0";
            }
            else
            {
                $key['online']="1";
            }
        }
        return response()->json($data);
    }

}
