<?php

namespace App\Http\Controllers;

use App\user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
// use Response;

class PageAboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function show(user $user,$email=null,$about=null)
    {
        //
        $user = Auth::user();
        $email = strstr($user['email'],"@",true);
        $about = 'active';
        return view('profile',compact('user','about'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(user $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //   
            $user = Auth::user();
            if($request -> input('email') == $user['email'] )
              {
                $emailvalidate = 'required|string|email|max:255';
              }
            else
              {
                $emailvalidate = 'required|string|email|max:255|unique:users';
              }
        $validator=Validator::make($request->Input(), [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'email' => $emailvalidate,
            'gender' => 'required',
            "birthday" => 'required|date|date_format:Y-m-d',

        ]);
        if ($validator->fails()) {
            $errors = $validator->getMessageBag();
            return response()->json($errors); 
         // withErrors exac errroner e vercnum i shnorhiv classi
        // witeInput exac arjeqnern e et tanum old ov view hatvacum tpum e
        }
        else{
            $age = date("y") - $request->input('year');
            User::where("id",$user['id'])
                  ->update([
                    'name' => $request-> name,
                    'surname' => $request -> surname,
                    'email' => $request -> email,
                    'gender' => $request -> gender ,
                    'birthday' => $request -> birthday
                 ]);
            Auth::user()->update([
                    'name' => $request -> name,
                    'surname' => $request -> surname,
                    'email' => $request -> email,
                    'gender' => $request -> gender,
                    'birthday' => $request -> birthday
                    ]);
           return "success";     

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(user $user)
    {
        //
    }
    public function ChangePassword(Request $request){

        $user = Auth::user();
        if (Hash::check($request -> oldpassword, $user['password'])) 
        {
           $validator=Validator::make($request->Input(), [
                        'password' => 'required|min:6|confirmed',
                        'password_confirmation' => 'required|min:6',
                    ]);
           if($validator->fails()) 
            {
             $errors = $validator->getMessageBag();
             return response()->json($errors); 
            }

           else
            {
              User::where('id',$user['id'])->update(["password"=>Hash::make($request->password)]);
              return 'success';
            }
       }
       else
       {
           $errors = ['oldpassword' =>['you entered the wrong password']];
           return response()->json($errors); 
      
       }
    }
}
