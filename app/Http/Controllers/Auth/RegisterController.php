<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Mail\OrderShipped;
use App\Mail\Verify;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(Request $request)
    {
        $validator=Validator::make($request->Input(), [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'gender' => 'required',
            'day'=>'required',
            'month' => 'required',
            'year' => "required"

        ]);
        if ($validator->fails()) {
            return redirect('/')
                        ->withErrors($validator)
                        ->withInput();
         // withErrors exac errroner e vercnum i shnorhiv classi
        // witeInput exac arjeqnern e et tanum old ov view hatvacum tpum e
        }
        else{
            $this->create($request->Input());
            return redirect('login');

        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {   
        // User::create tvyalnern nermucum e baza//
        
        $user = User::create([
            'name' => $data['name'],
            'surname' =>$data['surname'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'gender'=>$data['gender'],
            'birthday'=>$data['year']."-".$data['month']."-".$data['day']
        ]);
       
        \Mail::to($user) -> send(new Verify($user));

    }
    public function verify($id){
            $id = base64_decode($id);
            User::where('id', $id)
                ->update(['status' => 1]);
            return redirect('login');
        }
    
}
