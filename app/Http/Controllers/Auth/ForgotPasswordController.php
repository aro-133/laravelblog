<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Mail\OrderShipped;
class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    public function sendemail(Request $request){

         $email = $request ->Input('email');

         $validate = Validator::make($request ->Input(),
          ['email'=>'required|string|email|max:255']
          );

         if($validate -> fails()){
            return 'Wrong email address';
         }
         else{
            $user = User::where("email",$email)->first();
            if(!empty($user)){
            $code = rand(10000,99999);
            session(['code'=>$code]);
            session(['email' => $user['email']]);
            \Mail::to($user) -> send(new OrderShipped($code));
            return 1;
            }
            else{
              return 'Such user is not registered';
            }
         }
    }
    public function code(Request $request){
          if(session('code') == $request ->input('code')){
            session()->forget('code');
            return 1;
          }
          else{
            return 'The code is wrong';
          }
      }
    public function forgotpassword(Request $request){
        $validate = Validator::make($request -> input(),[
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',

        ]);
        if(!$validate -> fails()){
            $password = Hash::make($request->input('password'));
            User::where('email',session('email'))->update(['password'=>$password]);
            session()->forget('email');
        }
        else{
            return 'invalid password';
        }

    }
}
