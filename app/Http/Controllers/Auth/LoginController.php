<?php

namespace App\Http\Controllers\Auth;
use App\User;
use App\OnlineModel;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticate(Request $request,OnlineModel $online)
    {

        $credentials = $request->only('email','password');
        // $credentials['status'] = 1;
        $error = (object)[
            'message' => ''
        ];
        $remeber = $request->has("checked");
        $a = Auth::attempt($credentials, $remeber, $error);

        if ($a) {
            // Authentication passed...
            // $online->where('user_id', Auth::id())->delete();
            // $online->user_id= Auth::id();
            // $online->save();
            return redirect()->intended('/home');
        }
        else{   
                return redirect()->intended('/login')
                       ->withErrors(['verify' => $error->message]);
        }
    }
    public function logout(OnlineModel $online){
        // $online->where("user_id",Auth::id())
        //        ->update(["online"=>0]);
         Auth::logout();
        return redirect('/login');
    }
    protected function redirectTo($request)
        {
            return route('login');
        }

}
