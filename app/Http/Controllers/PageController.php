<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\ShareModel;
use App\LikeShareModel;
use App\CommentShareModel;
use App\FollowsPageModel;
use App\Friends;
use App\RequestModel;
use App\User;
use App\GuestModel;
use App\PostHideModel;
class PageController extends Controller
{
    //
  

    public static function getTimeline($user_id)
    {       

    	   $my_id = Auth::id();
    	   $timeline = ShareModel::where("user_id",$user_id)
                                  ->join('users',"users.id",'=','share.user_id')
                                  ->select('users.name',"users.surname","users.photo as avatar","users.photo",'share.*')
                                  ->orderBy('time','desc')
                                  ->get();
           foreach ($timeline as $key) {
                $post_id = $key['id'];
                $count = LikeShareModel::where('post_id',$post_id)
                                  ->groupBy('post_id')
                                  ->count('*');
                $key['like_count'] = $count;
                $count = CommentShareModel::where("post_id",$post_id)
                                   ->groupBy("post_id")
                                   ->count("*");
                $key['comment_count'] = $count;
                $count = LikeShareModel::where([
                                ['user_id',$my_id],
                                ['post_id',$post_id]
                            ])->count("*");
                $key['testlikeshare'] = $count;
                $count = PostHideModel::where([
                                ["user_id",Auth::id()],
                                ["post_id",$post_id]
                              ])->count("*");
                $key['post_hide'] = $count;
            }
        return $timeline;

    }
    public static function getFriends($user_id)
    {      
    	   $my_id = Auth::id();
    	   $users_1 = Friends::where("user2_id",$user_id)
                             ->pluck('user1_id');
           $users_2 = Friends::where("user1_id",$user_id)
                             ->pluck('user2_id');
           $friends = $users_1 -> concat($users_2);
           $friends = User::WhereIn('id',$friends)
                           ->where("id",'!=',$my_id)
                           ->get();
          if($friends ->IsNotEmpty())
          {
           foreach ($friends as $key ) 
             {

              $user_id  = $key['id'];
              if(RequestModel::where([
             	         ['user1_id',$user_id],
             	         ['user2_id',$my_id]
             	                    ])->count("*"))
             {
              $key['status'] = "acceptrequest";
            
             }
             elseif(RequestModel::where([
             	         ['user2_id',$user_id],
             	         ['user1_id',$my_id]
             	                    ])->count("*"))
             {
              $key['status'] = "cancelrequest";
              
             }

             elseif(Friends::where([
             	         ['user1_id',$my_id],
             	         ['user2_id',$user_id]
             	              ])->orWhere(function($query)use($user_id,$my_id){
                        $query -> Where([['user1_id',$user_id],['user2_id',$my_id]]);
                    })->count('*'))
             {
              $key['status'] = "friend";
             }
             else{
              $key['status'] = "unfriend";
           }
         }
       } 
       return $friends;
     }
     public function DeletePost(Request $request)
     {
         ShareModel::where("id",$request->post_id)
                   ->delete();

     }





     public function HidePost(Request $request)
     {
         PostHideModel::insert(["user_id" => Auth::id(),"post_id" => $request -> post_id]);
     }





     public function ShowLikes(Request $request){
         $user_id = LikeShareModel::where("post_id",$request -> post_id)
                                    ->pluck("user_id");
         $users = User::find($user_id);
         return response()->Json($users);
     }



     public function CancelRequest(Request $request)
     {   
         $id = Auth::id();
         $user_id = $request -> user_id;
         RequestModel::where([['user1_id',$id],['user2_id',$user_id]])
                      ->orWhere(function($query)use($id,$user_id){
                      $query->where([['user2_id',$id],["user1_id",$user_id]]);
                      })->delete();
     }


     public function AcceptRequest(Request $request)
     {   
         $id = Auth::id();
         $user_id = $request -> user_id;
         RequestModel::where([['user2_id',$id],['user1_id',$user_id]])
                      ->delete();
         Friends::insert(['user1_id'=>$id,'user2_id'=>$user_id]);
     }

     public function UnFriend(Request $request)
     {   
         $id = Auth::id();
         $user_id = $request -> user_id;
         Friends::where([['user1_id',$id],['user2_id',$user_id]])
                      ->orWhere(function($query)use($id,$user_id){
                      $query->where([['user2_id',$id],["user1_id",$user_id]]);
                      })->delete();
     }

     public function SendRequest(Request $request)
     {
         $id = Auth::id();
         $user_id = $request -> user_id;
         RequestModel::insert(['user1_id'=>$id,'user2_id'=>$user_id]);
     }

     public function Guest()
     {
         $id = Auth::id();
         $guest = GuestModel::join('users','users.id','=','guest.user1_id')
                             ->where("user2_id",$id) 
                             ->select('users.*','guest.date','guest.status')
                             ->get();
         GuestModel::where("user2_id",$id) 
                             ->update(['status'=>1]);
                             
         return response()->json($guest);
     }




}
