<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\OnlineModel;
use Illuminate\Http\Request;

class UserActiv
{


    protected $auth;

    public function __construct(Auth $auth)
    {
        // $this -> auth = $auth;

    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {     
      if(Auth::check()){
          if(OnlineModel::where('user_id',Auth::id())->get()->IsNotEmpty())
          {
            OnlineModel::where("user_id",Auth::id())->update(['lastactive'=>date("Y-m-d H:i:s")]);
          }
          else
          {
            OnlineModel::insert(['user_id'=>Auth::id(),'lastactive'=>date("Y-m-d H:i:s")]);
          }
      }

          return $next($request);
    }
}
