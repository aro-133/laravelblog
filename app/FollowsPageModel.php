<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FollowsPageModel extends Model
{
    //
    protected $table = 'follows';
}
